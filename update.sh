#!/usr/bin/env bash

# remove old cached files
find . -name "*.pyc" -exec rm -f {} \;

# make server see changes
touch casino/wsgi.py

python manage.py check
python manage.py migrate
python manage.py makemigrations
python manage.py migrate

# collect static files
python manage.py collectstatic --noinput
