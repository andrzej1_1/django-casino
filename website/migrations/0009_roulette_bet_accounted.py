# Generated by Django 2.0.6 on 2018-06-04 08:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0008_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='roulette_bet',
            name='accounted',
            field=models.BooleanField(default=False),
        ),
    ]
