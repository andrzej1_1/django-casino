from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import (
    User, Currency, User_Deposit, User_Withdraw, Roulette_Bet, RouletteRound,
    Seed, Message
)

admin.site.register(User, UserAdmin)
admin.site.register(Currency)
admin.site.register(User_Deposit)
admin.site.register(User_Withdraw)
admin.site.register(Roulette_Bet)
admin.site.register(RouletteRound)
admin.site.register(Seed)
admin.site.register(Message)
