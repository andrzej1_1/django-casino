from django.apps import AppConfig
from django.http import HttpRequest


class WebsiteConfig(AppConfig):
    name = 'website'

    def ready(self):
        from website import views
        request = HttpRequest()
        views.wamp_init(request)
