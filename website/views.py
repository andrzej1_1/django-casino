"""Controll GET and POST requests for all casino functionality."""
from autobahn.wamp.exception import ApplicationError
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.contrib.auth import views as auth_views
from django.contrib.auth import login, authenticate
from django.utils import timezone
from casino import settings
from website.exceptions import NegativeTokens
from website.models import (
    User, Currency, Roulette_Bet, RouletteRound, Seed, User_Withdraw, Message
)
from website.forms import SignUpForm
from autobahn_sync import publish, register

import autobahn_sync
import stripe
import json
import logging

logger = logging.getLogger(__name__)
stripe.api_key = settings.STRIPE_SECRET_KEY


class CustomLoginView(auth_views.LoginView):
    """Collect methods which extends django authentication functionality."""

    def form_valid(self, form):
        """Extend basic validation with user remember functionality.

        Check if remember checkbox was set by user and store session data
        in such case.
        """
        if self.request.POST.get('remember_me', None):
            self.request.session.set_expiry(0)
        return super().form_valid(form)


def signup(request):
    """Responsible for validation and creation of new users.

    Check if all required inputs are filled, if password and
    password confirmation are equal, if user with posted username
    already not exists and then create new user with possible friend username
    or blank string. After succesed registration proceed authentication
    and redirect to index path, otherwise return error messages to source
    registration form.
    """
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            friend = form.cleaned_data.get('friend')
            user = authenticate(username=username, password=raw_password)
            user.friend_username = friend
            user.save()
            login(request, user)
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})


def index(request):
    """Responsible for render welcome template.

    With render user authentication is checked, to show appropriate welcome
    text and advise for logged in or not authenticated user.
    """
    return render(request, 'website/index.html')


def roulette(request):
    """Responsible for prepare last bets and draw results.

    Prepare json with list of last draw results and
    """
    green_choice = Roulette_Bet._color_to_choice('green')
    red_choice = Roulette_Bet._color_to_choice('red')
    black_choice = Roulette_Bet._color_to_choice('black')

    unaccounted_bets = Roulette_Bet.unaccounted()
    green_bets = unaccounted_bets.filter(choice=green_choice).values_list(
        'amount', 'user_id__username', named=True
    )
    red_bets = unaccounted_bets.filter(choice=red_choice).values_list(
        'amount', 'user_id__username', named=True
    )
    black_bets = unaccounted_bets.filter(choice=black_choice).values_list(
        'amount', 'user_id__username', named=True
    )

    green_bets_json = json.dumps([bet._asdict() for bet in green_bets])
    red_bets_json = json.dumps([bet._asdict() for bet in red_bets])
    black_bets_json = json.dumps([bet._asdict() for bet in black_bets])

    current_bets_json = "{'green': " + green_bets_json + ",'red': " + red_bets_json + ", 'black': " + black_bets_json + "}"

    latest_draws = RouletteRound.objects.order_by('-id')[:10].values_list(
                    'number', flat=True)
    latest_draws_json = json.dumps(list(latest_draws))

    return render(request, 'website/roulette.html', {
        'latest_draws_json': latest_draws_json,
        'current_bets_json': current_bets_json,
    })


def deposit(request):
    """Allow user to exchange real money to tokens using deposit_checkout form.

    Firstly query Currency model for all elements and then prepare context
    dictionary with query result and public key from settings.py file.
    """
    currencies = Currency.objects.all()
    context = {
        "stripe_key": settings.STRIPE_PUBLIC_KEY,
        "currencies": currencies,
    }
    return render(request, 'website/deposit.html', context)


def deposit_checkout(request):
    """Responsible for connect stripe module with requested user model.

    Accept only POST requests with correct type of amount and currency fields,
    then try to cast amount into float value and use get_tokens_amount from
    Currenct model to return adequate number of tokens. During those operations
    catch situation when currency from request does not exist in base.
    After succesful form validation use Charge model from stripe module
    to complete transaction details. Catch every error raised during this
    operation and render deposit_error template to user.

    At the end of succesfull transaction, change token_count of requesting
    user and try to reward his friend user, after render deposit_succes
    template.
    """
    if request.method != "POST":
        return HttpResponseNotFound('<h1>Page not found</h1>')

    amount_str = request.POST.get("amount")
    currency = request.POST.get("currency")

    try:
        amount = float(amount_str)
        tokens_amount = Currency.get_tokens_amount(currency, amount)
    except Currency.DoesNotExist:
        return render(request, 'website/deposit_error.html', {
            'error_message': 'Currency does not exist',
        })
    except TypeError:
        return render(request, 'website/deposit_error.html', {
            'error_message': 'Wrong data submitted'
        })

    try:
        stripe.Charge.create(
            source=request.POST.get("stripeToken"),
            currency=currency,
            amount=amount_str,
        )
    except stripe.error.CardError as ce:
        return render(request, 'website/deposit_error.html')

    request.user.addTokens(tokens_amount)
    request.user.reward_friend(tokens_amount)
    request.user.save()
    return render(request, 'website/deposit_success.html')


def withdraw(request):
    """Responsible for create singal user will to withdraw money for admin.

    Firstly query Currency model for all elements and then in case of GET
    request render template using quered currencies, otherwise check user
    authentication and validate withdraw-amount, withdraw-currency and
    address fields. During validation and creation of new User_Withdraw
    object Currency.DoesNotExist and NegativeTokens will be catched and
    adequate template will be render.

    At the end of successful path new User_Withdraw object should be
    persist in database and user token_count decreased.
    """
    currencies = Currency.objects.all()
    context = {
            "currencies": currencies,
        }
    if request.method == "GET":
        return render(request, 'website/withdraw.html', context)
    else:
        if not request.user.is_authenticated:
            return render(request, 'website/withdraw.html',
                          {'logout': 'logout'})

        tokens_amount = request.POST.get("withdraw-amount")
        currency_name = request.POST.get("withdraw-currency")
        address = request.POST.get("address")

        try:
            tokens = int(tokens_amount)
            _currency = Currency.objects.get(name=currency_name)
            amount_from_tokens = Currency.get_withdraw_amount(currency_name,
                                                              tokens)
            request.user.decrease_tokens(tokens)
            User_Withdraw.objects.create(amount=amount_from_tokens,
                                         address=address,
                                         user_id=request.user,
                                         currency_id=_currency,
                                         withdraw_time=timezone.now())
        except Currency.DoesNotExist:
            context['nocurrency'] = 'nocurrency'
            return render(request, 'website/withdraw.html', context)
        except NegativeTokens:
            context['negative'] = 'negative'
            return render(request, 'website/withdraw.html', context)
        except ValueError:
            context['data'] = "Value Error"
            return render(request, 'website/withdraw.html', context)

        context['message'] = 'message'
        request.user.save()

        return render(request, 'website/withdraw.html', context)


def seed(request):
    """Responsible for visualization of all seeds.

    Firstly collect all seed elements, visuable and non visuable,
    and then use them to render template.
    """
    seed_list = Seed.objects.all()
    return render(request, 'website/seed.html', {'seed_list': seed_list})


def provably(request):
    """Allow user to check draws correctness."""
    return render(request, 'website/provably.html')


def shoutbox(request):
    """Allow users to read latest messages and write short messages.

    Firstly collect some latest messages from Message model, then use
    them to prepare context variable. GET request render template with
    collected context, other requests check user authentication and
    add new message using message-text form field.
    """
    latest_messages = Message.objects.order_by('-id')[:100].values_list(
                    'text', 'user_id__username', named=True)
    latest_messages_json = json.dumps(
        [msg._asdict() for msg in latest_messages]
    )

    context = {
        'latest_messages_json': latest_messages_json,
    }

    return render(request, 'website/shoutbox.html', context)


def wamp_init(request):

    try:
        autobahn_sync.run(url="ws://crossbar:8080/ws", realm="realm1")
    except Exception:
        print('Unable to run autobahn.')

    try:
        @register('com.casino.play_round')
        def play_round_wrapper():
            return play_round()
    except Exception:
        print('Unable to register.')

    try:
        @register('com.casino.wamp_bet')
        def wamp_bet_wrapper(user_id, amount, color):
            return User.wamp_bet(user_id, amount, color)
    except Exception:
        print('Unable to register.')

    try:
        @register('com.casino.send_message_wamp')
        def send_message_wamp_wrapper(user_id, text):
            return Message.send_message_wamp(user_id, text)
    except Exception:
        print('Unable to register.')

    return HttpResponse("OK")
#
# WAMP functions
#


def play_round():
    """Responsible for connect latest roullete result with vue module."""
    round = RouletteRound.go()
    publish(u'com.casino.roulette_draw', round.number)
    return True
