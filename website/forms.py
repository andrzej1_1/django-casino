from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model


class SignUpForm(UserCreationForm):
    """Prepares help texts, class and placeholder attributes.

    Define methods to increase and decrese token_count amount,
    betting and check if bet is possible.
    """

    username = forms.CharField(max_length=50, required=True,
        label='', help_text='Required. Inform unique username',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Username...'
        }))

    first_name = forms.CharField(max_length=30, required=False,
        label='', help_text='Optional',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'First name...'
        }))

    last_name = forms.CharField(max_length=30, required=False,
        label='', help_text='Optional',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Last name...'
        }))

    friend = forms.CharField(max_length=150, required=False,
        label='',
        help_text='Fraction of your first deposit result will be send to friend',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Friend Username...'
        }))

    email = forms.EmailField(max_length=254, required=True,
        label='', help_text='Required. Inform a valid email unique address.',
        widget=forms.EmailInput(attrs={
            'class': 'form-control',
            'placeholder': 'Email...'
        }))

    password1 = forms.CharField(required=True,
        label='',
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'placeholder': 'Password...'
        }))

    password2 = forms.CharField(required=True,
        label='',
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'placeholder': 'Confirm password...'
        }))

    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'friend',
            'email', 'password1', 'password2')
