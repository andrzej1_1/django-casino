from django.test import TestCase
from django.utils import timezone
from casino.settings import STRIPE_PUBLIC_KEY
from unittest.mock import patch
from website.models import (Currency, Roulette_Bet, RouletteRound,
    Seed, User, User_Withdraw, Message)

import json
import random
import stripe


class SeedPageTest(TestCase):

    def test_uses_seed_template(self):
        response = self.client.get('/seed')
        self.assertTemplateUsed(response, 'website/seed.html')

    def test_seed_page_uses_seeds(self):
        seed1 = Seed.objects.create(seed='123')
        seed2 = Seed.objects.create(seed='987')
        response = self.client.get('/seed')
        seeds = response.context['seed_list']
        self.assertEqual(
            list(seeds),
            [seed1, seed2]
        )

    def test_provably_template(self):
        response = self.client.get('/provably')
        self.assertTemplateUsed(response, 'website/provably.html')


class RegisterationTest(TestCase):
    TEST_USERNAME = 'john'
    TEST_FIRSTNAME = 'Johny'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'
    TEST_PASSWORD_SHORT = 'short'

    def test_uses_register_template(self):
        response = self.client.get('/signup')
        self.assertTemplateUsed(response, 'registration/signup.html')

    def test_register_fails_blank_username(self):
        response = self.client.post('/signup', data={
            "password1": self.TEST_PASSWORD,
            "password2": self.TEST_PASSWORD,
            "email": self.TEST_EMAIL,
        })
        self.assertFormError(response, 'form', 'username',
        	'This field is required.')

    def test_register_fails_blank_password(self):
        response = self.client.post('/signup', data={
            "username": self.TEST_USERNAME,
            "password2": self.TEST_PASSWORD,
            "email": self.TEST_EMAIL,
        })
        self.assertFormError(response, 'form', 'password1',
        	'This field is required.')

    def test_register_fails_blank_password_confirmation(self):
        response = self.client.post('/signup', data={
            "username": self.TEST_USERNAME,
            "password1": self.TEST_PASSWORD,
            "email": self.TEST_EMAIL,
        })
        self.assertFormError(response, 'form', 'password2',
        	'This field is required.')

    def test_register_fails_blank_email(self):
        response = self.client.post('/signup', data={
            "username": self.TEST_USERNAME,
            "password1": self.TEST_PASSWORD,
            "password2": self.TEST_PASSWORD,
        })
        self.assertFormError(response, 'form', 'email',
        	'This field is required.')

    def test_register_fails_user_already_exists(self):
        User.objects.create_user(username=self.TEST_USERNAME,
                                 email=self.TEST_EMAIL,
                                 password=self.TEST_PASSWORD)
        response = self.client.post('/signup', data={
            "username": self.TEST_USERNAME,
            "password1": self.TEST_PASSWORD,
            "password2": self.TEST_PASSWORD,
            "email": self.TEST_EMAIL,
            "first_name": self.TEST_FIRSTNAME,
        })

        self.assertContains(response,
            'A user with that username already exists.')

    def test_register_fails_passwords_not_match(self):
        response = self.client.post('/signup', data={
            "username": self.TEST_USERNAME,
            "password1": self.TEST_PASSWORD,
            "password2": "SimplyWrongPassword123!",
            "email": self.TEST_EMAIL,
            "first_name": self.TEST_FIRSTNAME,
        })
        print(response.content)
        self.assertContains(response,
            "The two password fields didn&#39;t match.")

    def test_register_fails_too_short_password(self):
        response = self.client.post('/signup', data={
            "username": self.TEST_USERNAME,
            "password1": self.TEST_PASSWORD_SHORT,
            "password2": self.TEST_PASSWORD_SHORT,
            "email": self.TEST_EMAIL,
            "first_name": self.TEST_FIRSTNAME,
        })

        self.assertContains(response,
        	'This password is too short.')

    def test_register_fails_password_too_similar_username(self):
        response = self.client.post('/signup', data={
            "username": self.TEST_USERNAME,
            "password1": self.TEST_USERNAME,
            "password2": self.TEST_USERNAME,
            "email": self.TEST_EMAIL,
            "first_name": self.TEST_FIRSTNAME,
        })

        self.assertContains(response,
        	'The password is too similar to the username.')

    def test_register_pass(self):
        response = self.client.post('/signup', data={
            "username": self.TEST_USERNAME,
            "password1": self.TEST_PASSWORD,
            "password2": self.TEST_PASSWORD,
            "email": self.TEST_EMAIL,
            "first_name": self.TEST_FIRSTNAME,
        })

        self.assertRedirects(response, "/")

    def test_register_pass_with_friend_field(self):
        friend = 'myFriend'

        response = self.client.post('/signup', data={
            "username": self.TEST_USERNAME,
            "password1": self.TEST_PASSWORD,
            "password2": self.TEST_PASSWORD,
            "email": self.TEST_EMAIL,
            "friend": friend,
            "first_name": self.TEST_FIRSTNAME,
        })

        self.assertRedirects(response, "/")

        user = User.objects.get(username=self.TEST_USERNAME)
        self.assertEqual(friend, user.friend_username)



class LoginPageTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    def test_user_can_login_with_valid_data(self):
        User.objects.create_user(username=self.TEST_USERNAME,
                                 email=self.TEST_EMAIL,
                                 password=self.TEST_PASSWORD)
        response = self.client.post('/login', {
            'username': self.TEST_USERNAME,
            'password': self.TEST_PASSWORD
        })
        self.assertRedirects(response, '/')

    def test_user_cannot_login_with_invalid_password(self):
        User.objects.create_user(username=self.TEST_USERNAME,
                                 email=self.TEST_EMAIL,
                                 password=self.TEST_PASSWORD)
        response = self.client.post('/login', {
            'username': self.TEST_USERNAME,
            'password': (self.TEST_PASSWORD + ' ')
        })
        self.assertContains(response,
                            "Your username and password didn't match")


class HomePageTest(TestCase):

    def test_uses_layout_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'website/layout.html')


class DepositPageTest(TestCase):

    def test_uses_layout_template(self):
        response = self.client.get('/deposit')
        self.assertTemplateUsed(response, 'website/deposit.html')

    def test_deposit_page_lists_currencies(self):
        currency1 = Currency.objects.create(name="USD", rate=332)
        currency2 = Currency.objects.create(name="PLN", rate=123)
        response = self.client.get('/deposit')
        currencies = response.context['currencies']
        self.assertEqual(
            list(currencies),
            [currency1, currency2]
        )
        self.assertIn(currency1.name, response.content.decode())
        self.assertIn(currency2.name, response.content.decode())
        self.assertIn(str(currency1.rate), response.content.decode())
        self.assertIn(str(currency2.rate), response.content.decode())

    def test_deposit_page_has_stripe_public_key(self):
        response = self.client.get('/deposit')
        key = response.context['stripe_key']
        self.assertEqual(key, STRIPE_PUBLIC_KEY)


class DepositCheckoutPageTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    @patch('stripe.Charge.create')
    def test_call_stripe_create(self, mock_create):

        # fixture
        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)

        mock_create.side_effect = stripe.error.CardError('exception',
                                                         'param', 123)
        _amount_str = str(random.randint(0, 10000) / 100)
        _stripeToken = "tok_KPte7942xySKBKyrBu11yEpf"
        _currency = curr_.name

        self.client.post('/checkout', data={
            "amount": _amount_str,
            "stripeToken": _stripeToken,
            "currency": curr_.name,
        })

        self.assertEqual(mock_create.called, True)
        args, kwargs = mock_create.call_args
        self.assertEqual(kwargs['amount'], _amount_str)
        self.assertEqual(kwargs['currency'], _currency)
        self.assertEqual(kwargs['source'], _stripeToken)

    def test_return_404_for_non_POST(self):

        response = self.client.get('/checkout')
        self.assertEqual(response.status_code, 404)

        response = self.client.patch('/checkout')
        self.assertEqual(response.status_code, 404)

    @patch('stripe.Charge.create')
    def test_show_error_page_for_invalid_data(self, mock_create):

        mock_create.side_effect = stripe.error.CardError('exception',
                                                         'param', 123)
        _amount = str(random.randint(0, 10000) / 100)
        _stripeToken = "tok_KPte7942xySKBKyrBu11yEpf"
        _currency = "PLN"

        response = self.client.post('/checkout', data={
            "amount": _amount,
            "stripeToken": _stripeToken,
            "currency": _currency,
        })

        self.assertTemplateUsed(response, 'website/deposit_error.html')

    @patch('stripe.Charge.create')
    def test_add_user_tokens_for_valid_data(self, mock_create):

        # fixture
        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)

        mock_create.return_value = 'OK :)'
        _amount = random.randint(0, 10000) / 100
        _stripeToken = "tok_KPte7942xySKBKyrBu11yEpf"

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD)
        self.client.force_login(user)

        response = self.client.get('/')
        pre_tokens = user.token_count

        response = self.client.post('/checkout', data={
            "amount": str(_amount),
            "stripeToken": _stripeToken,
            "currency": curr_.name,
        })

        self.assertTemplateUsed(response, 'website/deposit_success.html')
        user.refresh_from_db()
        post_tokens = user.token_count

        expected_delta = Currency.get_tokens_amount(curr_.name, _amount)
        self.assertEqual(pre_tokens + expected_delta, post_tokens)


class WithdrawPageTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'
    ADDRESS = 'Walowa street'

    def test_uses_layout_template(self):
        response = self.client.get('/withdraw')
        self.assertTemplateUsed(response, 'website/withdraw.html')

    def test_withdraw_page_lists_currencies(self):
        currency1 = Currency.objects.create(name="USD", rate=332)
        currency2 = Currency.objects.create(name="PLN", rate=123)
        response = self.client.get('/withdraw')
        currencies = response.context['currencies']
        self.assertEqual(
            list(currencies),
            [currency1, currency2]
        )

    def test_create_new_withdraw(self):
        # fixture
        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)
        withdraws_before = User_Withdraw.objects.count()

        _tokens = 200
        _amount = 100
        _currency = curr_.name

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                    email=self.TEST_EMAIL,
                                    password=self.TEST_PASSWORD,
                                    token_count=_tokens)

        self.client.force_login(user)

        response = self.client.post('/withdraw', data={
            "withdraw-amount": _amount,
            "withdraw-currency": _currency,
            "address": self.ADDRESS,
        })

        user.refresh_from_db()
        message = "System zarejestrował wypłatę, jeżeli pieniądze nie zostaną "
        "przelane w ciągu najbliższych 31 dni roboczych skontaktuj się z administratorem!"
        withdraws_after = User_Withdraw.objects.count()
        new_withdraw = User_Withdraw.objects.latest('withdraw_time')
        expected_amount = round(Currency.get_withdraw_amount(_currency, _amount), 2)

        self.assertTemplateUsed(response, 'website/withdraw.html')
        self.assertEqual(_tokens - _amount, user.token_count)
        self.assertEqual(withdraws_before, withdraws_after - 1)
        self.assertEqual(self.TEST_USERNAME, new_withdraw.user_id.username)
        self.assertEqual(expected_amount, new_withdraw.amount)
        self.assertEqual(self.ADDRESS, new_withdraw.address)
        self.assertContains(response, message)

    def test_wrong_currency_not_create_withdraw(self):
        # fixture
        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)
        withdraws_before = User_Withdraw.objects.count()

        _tokens = random.randint(2, 10000)
        _amount = _tokens - 1
        _currency = "XD"

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                    email=self.TEST_EMAIL,
                                    password=self.TEST_PASSWORD,
                                    token_count=_tokens)

        self.client.force_login(user)

        response = self.client.post('/withdraw', data={
            "withdraw-amount": _amount,
            "withdraw-currency": _currency,
            "address": self.ADDRESS,
        })

        user.refresh_from_db()
        message = "Currency does not exist!"
        withdraws_after = User_Withdraw.objects.count()
        self.assertTemplateUsed(response, 'website/withdraw.html')
        self.assertEqual(_tokens, user.token_count)
        self.assertEqual(withdraws_before, withdraws_after)
        self.assertContains(response, message)

    def test_wrong_amount_type_not_create_withdraw(self):
        # fixture
        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)
        withdraws_before = User_Withdraw.objects.count()

        _tokens = random.randint(2, 10000)
        _amount = "Not a number"
        _currency = curr_.name

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                    email=self.TEST_EMAIL,
                                    password=self.TEST_PASSWORD,
                                    token_count=_tokens)

        self.client.force_login(user)

        response = self.client.post('/withdraw', data={
            "withdraw-amount": _amount,
            "withdraw-currency": _currency,
            "address": self.ADDRESS,
        })

        user.refresh_from_db()
        message = "Wrong data!"
        withdraws_after = User_Withdraw.objects.count()
        self.assertTemplateUsed(response, 'website/withdraw.html')
        self.assertEqual(_tokens, user.token_count)
        self.assertEqual(withdraws_before, withdraws_after)
        self.assertContains(response, message)

    def test_not_enough_tokens_not_create_withdraw(self):
        # fixture
        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)
        withdraws_before = User_Withdraw.objects.count()

        _tokens = random.randint(2, 10000)
        _amount = 10001
        _currency = curr_.name

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                    email=self.TEST_EMAIL,
                                    password=self.TEST_PASSWORD,
                                    token_count=_tokens)

        self.client.force_login(user)

        response = self.client.post('/withdraw', data={
            "withdraw-amount": _amount,
            "withdraw-currency": _currency,
            "address": self.ADDRESS,
        })

        user.refresh_from_db()
        message = "Posiadasz zbyt mało tokenów!"
        withdraws_after = User_Withdraw.objects.count()
        self.assertTemplateUsed(response, 'website/withdraw.html')
        self.assertEqual(_tokens, user.token_count)
        self.assertEqual(withdraws_before, withdraws_after)
        self.assertContains(response, message)

    def test_not_logged_tried_withdraw(self):
        # fixture
        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)
        withdraws_before = User_Withdraw.objects.count()

        _tokens = random.randint(2, 10000)
        _amount = _tokens + 1
        _currency = curr_.name

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                    email=self.TEST_EMAIL,
                                    password=self.TEST_PASSWORD,
                                    token_count=_tokens)

        response = self.client.post('/withdraw', data={
            "withdraw-amount": _amount,
            "withdraw-currency": _currency,
            "address": self.ADDRESS,
        })

        user.refresh_from_db()
        message = "Tylko zalogowany użytkownik może dokonać wypłaty!"
        withdraws_after = User_Withdraw.objects.count()
        self.assertTemplateUsed(response, 'website/withdraw.html')
        self.assertEqual(_tokens, user.token_count)
        self.assertEqual(withdraws_before, withdraws_after)
        self.assertContains(response, message)


class RoulettePageTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    def test_uses_layout_template(self):
        response = self.client.get('/roulette')
        self.assertTemplateUsed(response, 'website/roulette.html')

    def test_display_bet_message_when_logged(self):
        User.objects.create_user(username=self.TEST_USERNAME,
                                 email=self.TEST_EMAIL,
                                 password=self.TEST_PASSWORD)
        response = self.client.post('/login', {
            'username': self.TEST_USERNAME,
            'password': (self.TEST_PASSWORD)
        })
        response = self.client.get('/roulette')
        self.assertContains(response,
                            "Bet color john!")

    def test_display_login_message_when_not_logged(self):
        response = self.client.get('/roulette')
        self.assertContains(response,
                            "Log in to bet!")

    def test_contain_bet_buttons(self):
        response = self.client.get('/roulette')
        colors = ["red", "green", "black"]
        for color in colors:
            self.assertIn(
                f"id=\"bet_{color}\" v-on:click=\"bet(",
                response.content.decode()
            )

    def test_contain_element_with_draw_history_id(self):
        response = self.client.get('/roulette')
        self.assertIn('id="draw_history"', response.content.decode())

    def test_use_latest_numbers_as_json_in_context(self):

        latest_draws = RouletteRound.objects.order_by('-id')[:10].values_list(
                        'number', flat=True)

        response = self.client.get('/roulette')
        data = response.context['latest_draws_json']
        self.assertEqual(data, json.dumps(list(latest_draws)))

    def test_contain_latest_numbers_as_vue_data(self):
        response = self.client.get('/roulette')
        self.assertIn('vue_latest_draws', response.content.decode())

    def test_use_currernt_bets_as_json_in_context(self):

        green_choice = Roulette_Bet._color_to_choice('green')
        red_choice = Roulette_Bet._color_to_choice('red')
        black_choice = Roulette_Bet._color_to_choice('black')

        unaccounted_bets = Roulette_Bet.unaccounted()
        green_bets = unaccounted_bets.filter(choice=green_choice).values_list(
            'amount', 'user_id__username', named=True
        )
        red_bets = unaccounted_bets.filter(choice=red_choice).values_list(
            'amount', 'user_id__username', named=True
        )
        black_bets = unaccounted_bets.filter(choice=black_choice).values_list(
            'amount', 'user_id__username', named=True
        )

        green_bets_json = json.dumps([bet._asdict() for bet in green_bets])
        red_bets_json = json.dumps([bet._asdict() for bet in red_bets])
        black_bets_json = json.dumps([bet._asdict() for bet in black_bets])

        current_bets_json = "{'green': " + green_bets_json + ",'red': " + red_bets_json + ", 'black': " + black_bets_json + "}"

        # Now we can check
        response = self.client.get('/roulette')
        data = response.context['current_bets_json']
        self.assertEqual(data, current_bets_json)

    def test_contain_current_bets_as_vue_data(self):
        response = self.client.get('/roulette')
        self.assertIn('vue_current_bets', response.content.decode())


class ShoutboxPageTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'
    USER_MESSAGE = 'I am the best quickbetter mate'
    FIRST_MESSAGE = 'No, you are not'
    JS_INJECTION = "<script>alert('Injected!');</script>"

    def test_uses_layout_template(self):
        response = self.client.get('/shoutbox')
        self.assertTemplateUsed(response, 'website/shoutbox.html')

    def test_read_shoutbox(self):
        # fixture
        user = User.objects.create_user(
            username=self.TEST_USERNAME,
            email=self.TEST_EMAIL,
            password=self.TEST_PASSWORD
        )

        Message.objects.create(text=self.FIRST_MESSAGE, user_id=user)

        response = self.client.get('/shoutbox')

        self.assertTemplateUsed(response, 'website/shoutbox.html')
        self.assertContains(response, self.FIRST_MESSAGE)

        self.client.force_login(user)
        response_logged = self.client.get('/shoutbox')

        self.assertTemplateUsed(response_logged, 'website/shoutbox.html')
        self.assertContains(response_logged, self.FIRST_MESSAGE)

    def test_use_latest_messages_as_json_in_context(self):

        latest_messages = Message.objects.order_by('-id')[:100].values_list(
                        'text', 'user_id__username', named=True)
        latest_messages_json = json.dumps(
            [msg._asdict() for msg in latest_messages]
        )

        response = self.client.get('/shoutbox')
        data = response.context['latest_messages_json']
        self.assertEqual(data, latest_messages_json)

    def test_contain_latest_numbers_as_vue_data(self):
        response = self.client.get('/shoutbox')
        self.assertIn('vue_latest_messages', response.content.decode())

    def test_contain_button_with_shout_function(self):
        response = self.client.get('/shoutbox')
        self.assertIn(
            'button v-on:click="send_message()" id="send_message_button"',
            response.content.decode()
        )


class LayoutTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    def test_contain_token_count_as_vue_data(self):
        user = User.objects.create_user(
            username=self.TEST_USERNAME,
            email=self.TEST_EMAIL,
            password=self.TEST_PASSWORD,
        )

        self.client.force_login(user)

        response = self.client.get('/')
        self.assertIn('vue_token_count', response.content.decode())
