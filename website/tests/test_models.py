from django.test import TestCase
from django.utils import timezone
from unittest import mock
from unittest.mock import patch
from website.models import (
    Currency, Roulette_Bet, RouletteRound, Seed, User, mutex,
    User_Deposit, User_Withdraw, Message, publish,
)
from website.exceptions import LockException, NegativeTokens, NotEnoughTokens

import math
import random
import secrets

randbelow = secrets.randbelow


def randbelow_with_log(range):
    global last_rand
    last_rand = randbelow(range)
    return last_rand


orig_publish = publish

publish_args = []
publish_kwargs = []


def publish_with_call_save(*args, **kwargs):
    publish_args.append(args)
    publish_kwargs.append(kwargs)
    result = orig_publish(*args, **kwargs)
    return result


def clear_publish_call_saves():
    publish_args.clear()
    publish_kwargs.clear()


class UserModelTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    def test_user_str(self):
        _token_count = 100
        _friend_name = 'jash'

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD)
        user.token_count = _token_count

        excepted = self.TEST_USERNAME + " has " + str(_token_count)
        self.assertEqual(excepted, user.__str__())

    def test_possible_decrease(self):
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                 email=self.TEST_EMAIL,
                                 password=self.TEST_PASSWORD)
        user.token_count = random.randrange(2, 1000)
        decrease_number = user.token_count - 1
        expected_tokens = user.token_count - decrease_number
        user.decrease_tokens(decrease_number)
        user.save()
        user.refresh_from_db()
        self.assertEqual(user.token_count, expected_tokens)

    def test_impossible_decrease(self):
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                 email=self.TEST_EMAIL,
                                 password=self.TEST_PASSWORD)
        user.token_count = random.randrange(2, 1000)
        decrease_number = user.token_count + 1
        expected_tokens = user.token_count
        with self.assertRaises(NegativeTokens):
            user.decrease_tokens(decrease_number)

        self.assertEqual(user.token_count, expected_tokens)


class SeedModelTest(TestCase):

    def test_seed_str(self):

        _seed = '123'
        _visible = False
        unvisible_seed = Seed.objects.create(seed=_seed, visible=_visible)

        excepted = 'Hidden'
        self.assertEqual(excepted, unvisible_seed.__str__())

        _seed = '123'
        _visible = True
        visible_seed = Seed.objects.create(seed=_seed, visible=_visible)

        excepted = _seed
        self.assertEqual(excepted, visible_seed.__str__())

    def test_can_set_visibilty(self):
        seed = Seed(visible=False)
        self.assertEqual(seed.visible, False)

        seed2 = Seed(visible=True)
        self.assertEqual(seed2.visible, True)

    def test_visible_is_optional_and_false_by_default(self):
        seed = Seed()
        self.assertEqual(seed.visible, False)

    def test_generate_is_creating_new_seed(self):
        Seed.generate()
        self.assertTrue(Seed.objects.all())

    def test_can_get_none_as_last(self):
        object = Seed.get_last_or_none()
        self.assertEqual(object, None)

    def test_can_get_last(self):
        Seed.generate()
        seed = Seed.generate()

        object = Seed.get_last_or_none()
        self.assertEqual(object, seed)


class CurrencyModelTest(TestCase):

    def test_currency_str(self):

        _rate = 5.25
        _name = 'PLN'
        curr = Currency.objects.create(name=_name,
                                        rate=_rate)

        excepted = _name + " - " + str(_rate)
        self.assertEqual(excepted, curr.__str__())

    def test_return_tokens_amount_for_valid_data(self):

        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)

        base_value = random.randrange(155, 389) / 100
        values = [
            base_value, base_value + 0.01, base_value + 0.5, base_value + 0.99,
        ]

        for value in values:
            tokens = Currency.get_tokens_amount(curr_.name, value)
            expcted_value = math.floor(value * (curr_.rate))
            self.assertEqual(tokens, expcted_value)

    def test_return_withdraw_amount_for_valid_data(self):

        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)

        base_token = random.randrange(1000, 2000)
        tokens = [
            base_token, base_token + 1, base_token + 100, base_token + 1000,
        ]

        for token in tokens:
            amounts = Currency.get_withdraw_amount(curr_.name, token)
            expected_amounts = round(token / (curr_.rate), 2)
            self.assertEqual(amounts, expected_amounts)

    def test_raise_exception_for_invalid_data(self):

        Currency.objects.create(name="JPY", rate=1434)
        Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)

        value = random.randrange(155, 389) / 100
        with self.assertRaises(Currency.DoesNotExist):
            Currency.get_tokens_amount("XD", value)

    def test_no_change_for_no_tokens(self):

        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)

        token = 0
        amount = Currency.get_tokens_amount(curr_.name, token)
        self.assertEqual(amount, 0)

    def test_raise_exception_for_invalid_tokens_amount(self):

        Currency.objects.create(name="JPY", rate=1434)
        curr_ = Currency.objects.create(name="PLN", rate=213)
        Currency.objects.create(name="GBP", rate=174)

        token = -1
        with self.assertRaises(NegativeTokens):
            Currency.get_withdraw_amount(curr_.name, token)


class BetModelTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    def test_bet_str(self):

        amount_bet = 100
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD)

        bet = Roulette_Bet.objects.create(amount=amount_bet, choice=1,
                                    bet_time=timezone.now(), user_id=user)

        excepted = self.TEST_USERNAME + " bet " + str(amount_bet)
        self.assertEqual(excepted, bet.__str__())

    def test_bet_decrease_token_amount(self):

        initial_tokens = random.randint(1, 100000)
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=initial_tokens)

        user.bet(1, "red")
        self.assertEqual(user.token_count, initial_tokens - 1)

    @patch('website.models.publish')
    def test_bet_publish_new_tokens_count(self, mocked_publish):

        # Mock publish function
        clear_publish_call_saves()
        mocked_publish.side_effect = publish_with_call_save

        tokens = 100
        user = User.objects.create_user(
            username=(self.TEST_USERNAME + 'b'),
            email=(self.TEST_EMAIL + 'b'),
            password=self.TEST_PASSWORD,
            token_count=tokens
        )
        user.bet(20, "green")
        expected_tokens = tokens - 20

        correct = False
        for call_args in publish_args:
            try:
                topic = 'com.casino.updates.user' + str(user.id)
                self.assertEqual(call_args[0], topic)
                self.assertIn({
                    'type': 'tokens_count',
                    'value': expected_tokens,
                }, call_args[1])
                # we passed all checks!
                correct = True
                break
            except Exception:
                pass
        self.assertTrue(correct)

    @patch('website.models.Roulette_Bet.objects.create')
    def test_bet_require_enough_tokens(self, mocked_create):

        initial_tokens = random.randint(1, 100000)
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=initial_tokens)
        too_much_tokens = initial_tokens + 1
        with self.assertRaises(NotEnoughTokens):
            user.bet(too_much_tokens, "red")
        self.assertFalse(mocked_create.called)
        self.assertEqual(user.token_count, initial_tokens)

    @patch('website.models.Roulette_Bet.objects.create')
    def test_bet_require_more_than_zero_tokens(self, mocked_create):
        initial_tokens = 100
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=initial_tokens)
        with self.assertRaises(NotEnoughTokens):
            user.bet(0, "red")
        self.assertFalse(mocked_create.called)
        self.assertEqual(user.token_count, initial_tokens)

    def test_bet_create_and_return_object(self):
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=100)
        bet = user.bet(1, "red")
        bet_db = Roulette_Bet.objects.latest('id')
        self.assertEqual(bet, bet_db)

    def test_bet_is_marked_as_non_accounted_by_default(self):
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=100)
        bet = user.bet(1, "red")
        self.assertEqual(bet.accounted, False)

    def test_can_find_unaccounted_bets(self):
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=100)
        bet1 = user.bet(1, "red")
        bet2 = user.bet(2, "red")
        unaccounted = Roulette_Bet.unaccounted()
        self.assertEqual(list(unaccounted), [bet1, bet2])

    def test_bet_convert_color_to_choice(self):
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=100)
        bet1 = user.bet(1, "green")
        bet2 = user.bet(1, "red")
        bet3 = user.bet(1, "black")
        self.assertEqual(bet1.choice, 0)
        self.assertEqual(bet2.choice, 1)
        self.assertEqual(bet3.choice, 2)

    def test_can_get_award_multiplier_for_color(self):
        correct_awards = {
            'red': 2,
            'green': 36,
            'black': 2,
        }
        for _color, _mult in correct_awards.items():
            mult = Roulette_Bet._color_to_award_multiplier(_color)
            self.assertEqual(mult, _mult)


class RouletteRoundModelTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    """Try to release mutex"""
    def setUp(self):
        try:
            mutex.release()
        except RuntimeError:
            pass

    def test_go_return_record(self):

        Seed.objects.create(seed='123')
        record = RouletteRound.go()
        self.assertIsInstance(record, RouletteRound)

    def test_create_record_with_number(self):

        Seed.objects.create(seed='123')
        round = RouletteRound.go()
        self.assertTrue(round)
        self.assertTrue(str(round.number).isdigit())

    @patch('secrets.randbelow')
    def test_use_safe_random_number_with_valid_range(self, mock_randbelow):

        mock_randbelow.side_effect = randbelow_with_log

        Seed.objects.create(seed='123')
        round = RouletteRound.go()

        # randbelow must be called with arg: 37
        #   -> the maximum rouelette number + 1
        self.assertEqual(mock_randbelow.called, True)
        args, kwargs = mock_randbelow.call_args
        self.assertEqual(args[0], 37)

        self.assertEqual(round.number, last_rand)

    def test_go_create_seed_if_none_exist(self):

        round = RouletteRound.go()
        last_seed = Seed.objects.latest('id')

        self.assertEqual(round.seed, last_seed)

    def test_go_create_seed_if_latest_used_up(self):

        Seed.MAX_USAGES = 2

        RouletteRound.go()
        round2 = RouletteRound.go()
        round3 = RouletteRound.go()

        self.assertNotEqual(round2.seed, round3.seed)

    def test_is_associated_with_latest_server_seed(self):

        seed = Seed.objects.create(seed='123')
        round = RouletteRound.go()

        self.assertEqual(round.seed, seed)

    # @patch('website.models.Seed.get_last_or_none')
    def test_is_safe_from_race_condtion(self):

        Seed.MAX_USAGES = 10

        import website
        orig_last = website.models.Seed.get_last_or_none
        RouletteRound.run_additional_race_call = True

        def mocked_last():
            if RouletteRound.run_additional_race_call:
                # set to False must be before go()
                RouletteRound.run_additional_race_call = False
                result = orig_last()  # must be before go()
                RouletteRound.go()
            else:
                result = orig_last()
            return result

        with mock.patch('website.models.Seed.get_last_or_none',
                        new=mocked_last):
            with self.assertRaises(LockException):
                RouletteRound.go()

        self.assertEqual(RouletteRound.objects.all().count(), 0)
        seeds = Seed.objects.all()
        self.assertEqual(seeds.count(), 0)

    def test_can_get_seed_usages(self):

        self.assertEqual(RouletteRound.seed_usages(123), 0)

        seed = Seed.generate()
        RouletteRound.objects.create(seed=seed, number=0)
        RouletteRound.objects.create(seed=seed, number=0)

        self.assertEqual(RouletteRound.seed_usages(seed.id), 2)

    def test_can_get_winning_choice(self):
        seed = Seed.generate()

        round_green_1 = RouletteRound.objects.create(seed=seed, number=0)

        round_black_1 = RouletteRound.objects.create(seed=seed, number=4)
        round_black_2 = RouletteRound.objects.create(seed=seed, number=13)
        round_black_3 = RouletteRound.objects.create(seed=seed, number=20)
        round_black_4 = RouletteRound.objects.create(seed=seed, number=31)

        round_red_1 = RouletteRound.objects.create(seed=seed, number=5)
        round_red_2 = RouletteRound.objects.create(seed=seed, number=14)
        round_red_3 = RouletteRound.objects.create(seed=seed, number=21)
        round_red_4 = RouletteRound.objects.create(seed=seed, number=32)

        green_choice = Roulette_Bet._color_to_choice("green")
        self.assertEqual(round_green_1.get_winning_choice(), green_choice)

        black_choice = Roulette_Bet._color_to_choice("black")
        self.assertEqual(round_black_1.get_winning_choice(), black_choice)
        self.assertEqual(round_black_2.get_winning_choice(), black_choice)
        self.assertEqual(round_black_3.get_winning_choice(), black_choice)
        self.assertEqual(round_black_4.get_winning_choice(), black_choice)

        red_choice = Roulette_Bet._color_to_choice("red")
        self.assertEqual(round_red_1.get_winning_choice(), red_choice)
        self.assertEqual(round_red_2.get_winning_choice(), red_choice)
        self.assertEqual(round_red_3.get_winning_choice(), red_choice)
        self.assertEqual(round_red_4.get_winning_choice(), red_choice)

    def test_go_reward_winners(self):
        tokens = 1000
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=tokens)

        bet_amount = {1: 16, 2: 25, 3: 36}
        bet1 = user.bet(16, "red")
        tokens = tokens - bet_amount[1]
        bet2 = user.bet(25, "green")
        tokens = tokens - bet_amount[2]
        bet3 = user.bet(36, "black")
        tokens = tokens - bet_amount[3]

        self.assertIsInstance(bet1, Roulette_Bet)
        self.assertIsInstance(bet2, Roulette_Bet)
        self.assertIsInstance(bet3, Roulette_Bet)

        round = RouletteRound.go()

        winning_color = Roulette_Bet._number_to_color(round.number)
        award_mult = Roulette_Bet._color_to_award_multiplier(winning_color)
        expected_tokens = tokens + (award_mult * bet_amount[round.get_winning_choice()])
        self.assertTrue(user.token_count, expected_tokens)

    def test_go_mark_bets_as_accounted(self):
        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=100)
        bet1 = user.bet(10, "red")
        bet2 = user.bet(10, "green")
        bet3 = user.bet(10, "black")

        RouletteRound.go()

        bet1.refresh_from_db()
        bet2.refresh_from_db()
        bet3.refresh_from_db()

        self.assertEqual(bet1.accounted, True)
        self.assertEqual(bet2.accounted, True)
        self.assertEqual(bet3.accounted, True)

    @patch('website.models.publish')
    @patch('secrets.randbelow')
    def test_go_notify_awared_user(self, mocked_randbelow, mocked_publish):

        # Mock publish function
        clear_publish_call_saves()
        mocked_publish.side_effect = publish_with_call_save

        # Mock randbelow
        number = 0
        mocked_randbelow.return_value = number
        # winning color should be green
        winning_color = Roulette_Bet._number_to_color(number)

        # Run test
        User.objects.create_user(
            username=self.TEST_USERNAME,
            email=self.TEST_EMAIL,
            password=self.TEST_PASSWORD,
            token_count=100
        )
        user2 = User.objects.create_user(
            username=(self.TEST_USERNAME + 'b'),
            email=(self.TEST_EMAIL + 'b'),
            password=self.TEST_PASSWORD,
            token_count=100
        )
        user2.bet(10, "green")
        user2.bet(10, "red")
        user2.bet(10, "black")

        award_mult = Roulette_Bet._color_to_award_multiplier(winning_color)
        award = 10 * award_mult
        print('Correct award', award)

        RouletteRound.go()

        correct = False
        for call_args in publish_args:
            try:
                topic = 'com.casino.updates.user' + str(user2.id)
                self.assertEqual(call_args[0], topic)
                self.assertIn({
                    'type': 'roulette_win',
                    'value': award,
                    'delayed': True,
                }, call_args[1])
                # we passed all checks!
                correct = True
                break
            except Exception:
                pass
        self.assertTrue(correct)

    @patch('website.models.User.bet')
    def test_wamp_bet_call_user_bet(self, mocked_bet):
        user = User.objects.create_user(
            username=self.TEST_USERNAME,
            email=self.TEST_EMAIL,
            password=self.TEST_PASSWORD,
            token_count=100
        )
        User.wamp_bet(user.id, 100, "green")
        self.assertTrue(mocked_bet.called)

    def test_wamp_bet_can_return_not_enough_tokens(self):
        tokens = 10
        user = User.objects.create_user(
            username=self.TEST_USERNAME,
            email=self.TEST_EMAIL,
            password=self.TEST_PASSWORD,
            token_count=tokens
        )
        result = User.wamp_bet(user.id, tokens+1, "green")
        self.assertEqual(result, {'type': 'not_enough_tokens'})


class DepositModelTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    def test_deposit_str(self):

        amount_ = 100.00
        curr_ = Currency.objects.create(name="PLN", rate=200)

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD)

        deposit = User_Deposit.objects.create(amount=amount_,
            deposit_time=timezone.now(), user_id=user, currency_id=curr_)

        excepted = self.TEST_USERNAME + " made " + str(amount_) + " deposit"
        self.assertEqual(excepted, deposit.__str__())


class WithdrawModelTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    def test_withdraw_str(self):

        curr_ = Currency.objects.create(name="PLN", rate=200)

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD)

        amount_bet = 5.0

        withdraw = User_Withdraw.objects.create(amount=amount_bet,
                address="address", user_id=user, currency_id=curr_,
                withdraw_time=timezone.now())

        excepted = self.TEST_USERNAME + " want to withdraw " + str(amount_bet)
        self.assertEqual(excepted, withdraw.__str__())


class MessageModelTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'
    MSG = 'Simple message'

    def test_withdraw_str(self):

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD)

        message = Message.objects.create(text=self.MSG, user_id=user)

        excepted = self.TEST_USERNAME + ": " + str(self.MSG)
        self.assertEqual(excepted, message.__str__())

    def test_send_message_wamp_create_object(self):
        user = User.objects.create_user(
            username=self.TEST_USERNAME,
            email=self.TEST_EMAIL,
            password=self.TEST_PASSWORD
        )
        msg = 'Hello!'
        Message.send_message_wamp(user.id, msg)
        messages = Message.objects.all()
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0].text, msg)

    @patch('website.models.Message.objects.create')
    def test_send_message_wamp_check_if_user_exist(self, mocked_create):
        result = Message.send_message_wamp(1, 'Hello!')
        self.assertEqual(mocked_create.called, False)
        self.assertEqual(result, {'type': 'unexpected_error'})

    def test_send_message_wamp_return_success(self):
        user = User.objects.create_user(
            username=self.TEST_USERNAME,
            email=self.TEST_EMAIL,
            password=self.TEST_PASSWORD
        )
        result = Message.send_message_wamp(user.id, 'Hello!')
        self.assertEqual(result, {'type': 'success'})

    @patch('website.models.publish')
    def test_send_message_wamp_publish_shoutbox_update(self, mocked_publish):
        msg = 'Test'
        user = User.objects.create_user(
            username=(self.TEST_USERNAME),
            email=(self.TEST_EMAIL),
            password=self.TEST_PASSWORD,
        )
        Message.send_message_wamp(user.id, msg)
        self.assertEqual(mocked_publish.called, True)
        args, kwargs = mocked_publish.call_args
        self.assertEqual(args[0], 'com.casino.shoutbox')
        self.assertIn({
            'type': 'message',
            'value': {
                'user_id__username': user.username,
                'text': msg,
            }
        }, args[1])


class FriendDepositModelTest(TestCase):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    FRIEND_USERNAME = 'jash'
    FRIEND_EMAIL = 'jash@casino.test'
    FRIEND_PASSWORD = 'Ve3yInt21st1ngPass4o5d'

    def test_can_reward_friend(self):

        initial_tokens = random.randint(1, 100000)
        friend = User.objects.create_user(username=self.FRIEND_USERNAME,
                                        email=self.FRIEND_EMAIL,
                                        password=self.FRIEND_PASSWORD,
                                        token_count=initial_tokens)

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        friend_username=self.FRIEND_USERNAME)

        friend_reward_count = random.randint(1, 10000)
        user.reward_friend(friend_reward_count)

        friend.refresh_from_db()

        expected_token_count = initial_tokens + int(friend_reward_count / 10)
        self.assertEqual(expected_token_count, friend.token_count)
        self.assertEqual('', user.friend_username)

    def test_cannot_reward_negative(self):

        initial_tokens = random.randint(1, 100000)
        friend = User.objects.create_user(username=self.FRIEND_USERNAME,
                                        email=self.FRIEND_EMAIL,
                                        password=self.FRIEND_PASSWORD,
                                        token_count=initial_tokens)

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        friend_username=self.FRIEND_USERNAME)

        friend_reward_count = - random.randint(1, 10000)
        user.reward_friend(friend_reward_count)

        friend.refresh_from_db()

        self.assertEqual(initial_tokens, friend.token_count)
        self.assertEqual(self.FRIEND_USERNAME, user.friend_username)

    def test_cannot_reward_non_exist_friend(self):

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        friend_username=self.FRIEND_USERNAME)

        friend_reward_count = - random.randint(1, 10000)
        user.reward_friend(friend_reward_count)

        self.assertEqual('', user.friend_username)

    def test_cannot_reward_himself(self):

        initial_tokens = random.randint(1, 100000)

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        friend_username=self.TEST_USERNAME,
                                        token_count=initial_tokens)

        friend_reward_count = random.randint(1, 10000)
        user.reward_friend(friend_reward_count)

        self.assertEqual(initial_tokens, user.token_count)
        self.assertEqual('', user.friend_username)

    def test_no_change_when_no_friend(self):

        initial_tokens = random.randint(1, 100000)

        user = User.objects.create_user(username=self.TEST_USERNAME,
                                        email=self.TEST_EMAIL,
                                        password=self.TEST_PASSWORD,
                                        token_count=initial_tokens)

        friend_reward_count = random.randint(1, 10000)
        user.reward_friend(friend_reward_count)

        self.assertEqual(initial_tokens, user.token_count)
        self.assertEqual('', user.friend_username)
