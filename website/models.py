"""Models and methods for all casino functionality."""
from autobahn_sync import publish
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.utils import timezone
from threading import Lock
from website.exceptions import LockException, NegativeTokens, NotEnoughTokens

import json
import logging
import math
import secrets
from decimal import Decimal

logger = logging.getLogger(__name__)
mutex = Lock()


class User(AbstractUser):
    """Add two fields to existing Django User model.

    Define methods to increase and decrese token_count amount,
    betting and check if bet is possible.
    """

    token_count = models.IntegerField(default=0)
    friend_username = models.CharField(max_length=150, default='')

    def __str__(self):
        """Simply present username and his token_count."""
        return self.username + " has " + str(self.token_count)

    def addTokens(self, number):
        """Increase user tokens amount watch over not to use negative value.

        self -- user whose token_count field  gonna be increased
        number -- tokens amount, must be integer

        In case negative number no changes happened.
        """
        int_num = int(number)
        if int_num > 0:
            self.token_count += int_num

    def decrease_tokens(self, number):
        """Decrease user tokens amount watch over not to set negative value.

        Keyword arguments:
        self -- user whose token_count field gonna be decreased
        number -- tokens amount, must be integer, cannot be greater
                than token_count

        In case number is greater than user token_count NegativeTokens
        exception raised, otherwise simply decrease token_count with number.
        """
        int_num = int(number)
        if self.token_count - int_num >= 0:
            self.token_count -= int_num
        else:
            raise NegativeTokens()

    def reward_friend(self, number):
        """Add bonus tokens to existing friend user using friend_username field.

        Keyword arguments:
        self -- user who adds bonus to existing his friend user
        number -- tokens amount, must be integer, will be divided by 10

        In case self and friend_username are equal, friend_username field
        will be erased.
        """
        int_num = int(number)
        print(self.friend_username)
        print(self.username)
        if(self.friend_username is not self.username and
           self.friend_username is not ''):
            try:
                friend = User.objects.get(username=self.friend_username)

                if int_num > 0:
                    friend_num = int_num / 10
                    friend.addTokens(friend_num)
                    friend.save()
                else:
                    return
            except User.DoesNotExist as e:
                print(e)

        self.friend_username = ''

    def couldBet(self, amount):
        """Check whatever user has enough token_count to make bet with argument amount.

        Keyword arguments:
        self -- user who checks if it is possible to make bet
        amount -- tokens amount, must be integer

        Case of negative amount has the same result as token_count field
        value less than amount value.
        """
        difference = self.token_count - amount
        if amount > 0 and difference >= 0:
            return True
        return False

    def bet(self, amount, color):
        """Create new Roulette_Bet object with amount and color specified in arguments.

        Keyword arguments:
        self -- user who makes a bet
        amount -- tokens amount, must be integer,
                specify Roulette_Bet amount field
        choice -- tokens amount, must be integer,
                specify Roulette_Bet choice field

        If couldBet is False, then NotEnoughTokens is throw and there is no
        change in database, otherwise create new Roulette_Bet in database,
        do not check if amount or color is correct.
        """
        if not self.couldBet(amount):
            raise NotEnoughTokens()
        self.decrease_tokens(amount)
        self.save()
        choice = Roulette_Bet._color_to_choice(color)
        object = Roulette_Bet.objects.create(
            amount=amount,
            choice=choice,
            user_id=self,
            bet_time=timezone.now()
        )
        publish(u'com.casino.updates.user' + str(self.id), [
            {'type': 'tokens_count', 'value': self.token_count},
        ])
        object_json = {
            'amount': object.amount,
            'user_id__username': object.user_id.username,
        }
        publish(u'com.casino.current_bets', [
            {'type': 'bet', 'color': color, 'value': object_json},
        ])
        return object

    # WAMP function
    @classmethod
    def wamp_bet(cls, user_id, color, amount):
        """Enable concurrent bet for multiple users.

        Keyword arguments:
        cls -- enable connect to User model,
        user_id -- should be integer,
        color -- should be integer in range 1:3,
        amount -- should be positive integer,

        No return in case of not found user by user_id, other errors
        returned by value of key 'type' in return dictionary.
        """
        try:
            user_id = int(user_id)
            user = cls.objects.get(id=user_id)
            user.bet(color, amount)
            return {'type': 'success'}
        except cls.DoesNotExist:
            pass
        except NotEnoughTokens:
            return {'type': 'not_enough_tokens'}
        return {'type': 'unexpected_error'}


class Currency(models.Model):
    """Store currencies with specified name and rate to token amount."""

    name = models.CharField(max_length=30)
    rate = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        """Simply present currency name and it's rate."""
        return self.name + " - " + str(self.rate)

    @classmethod
    def get_tokens_amount(cls, currency_name, value):
        """Convert value in specified currency to tokens.

        Keyword arguments:
        cls -- enable connect to Currency model,
        currency_name -- allow to get specified currency,
        value -- float value represents amount of real money,

        Could raise Currency.DoesNotExist exception.
        Token value is rounded down after value multiplication by rate.
        """
        curr = cls.objects.get(name=currency_name)
        tokens = value * float(curr.rate)
        tokens_floor = math.floor(tokens)
        return tokens_floor

    @classmethod
    def get_withdraw_amount(cls, currency_name, tokens):
        """Convert tokens to amount of money in specified currency.

        Keyword arguments:
        cls -- enable connect to Currency model,
        currency_name -- allow to get specified currency,
        tokens -- integer value represents number of tokens,

        Could raise Currency.DoesNotExist exception and NegativeTokens
        exception.
        Returned object is casted to Decimal with two places precision.
        """
        curr = cls.objects.get(name=currency_name)
        if tokens < 0:
            raise NegativeTokens()

        value = Decimal(round(tokens / float(curr.rate), 2))
        return value


class User_Deposit(models.Model):
    """Represent single money deposit made by user using casino.

    Define fields to store amount of money, using Decimal field with
    two places precision and maximal six digits, time of deposit creation,
    and connect every deposit with user and used currency.
    """

    amount = models.DecimalField(max_digits=6, decimal_places=2)
    deposit_time = models.DateTimeField()
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    currency_id = models.ForeignKey(
        Currency,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        """Simply present name of user connected with deposit and amount."""
        return self.user_id.username + " made " + str(self.amount) + " deposit"


class User_Withdraw(models.Model):
    """Represent user's willingness to withdraw money.

    Define fields to store amount of money, using Decimal field with
    two places precision and maximal six digits, time when withdraw will
    was signaled and connect every withdraw with user and used currency.
    """

    amount = models.DecimalField(max_digits=6, decimal_places=2)
    address = models.CharField(max_length=100)
    withdraw_time = models.DateTimeField()
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    currency_id = models.ForeignKey(
        Currency,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        """Simply present name of user connected with withdraw and amount."""
        return self.user_id.username + " want to withdraw " + str(self.amount)


class Roulette_Bet(models.Model):
    """Represent single correct bet made by user during roullet game.

    Store amount which mean number of tokens, choice is number representaion
    of colors possible to bet also used accounted boolean field which
    determines if bet is still active or just obsolete.
    """

    amount = models.IntegerField()
    choice = models.IntegerField()
    bet_time = models.DateTimeField()
    user_id = models.ForeignKey(  # This should be just 'user'
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    accounted = models.BooleanField(default=False)

    def __str__(self):
        """Simply present name of user connected with bet and amount."""
        return self.user_id.username + " bet " + str(self.amount)

    @classmethod
    def unaccounted(cls):
        """Return only unaccounted bets, possible empty table."""
        objects = cls.objects.filter(accounted=False).all()
        return objects

    @classmethod
    def _color_to_choice(cls, color):
        """Map string represantion of color into integer representation."""
        mapping = {"green": 0, "red": 1, "black": 2}
        choice = mapping.get(color, None)
        return choice

    @classmethod
    def _color_to_award_multiplier(cls, color):
        """Return awadd multiplier for given color"""
        mapping = {"green": 36, "red": 2, "black": 2}
        choice = mapping.get(color, 1)
        return choice

    @classmethod
    def _number_to_color(cls, number):
        """Check color of number from roulette wheel.

        Keyword arguments:
        number -- Integer value, should be from 0 to 36

        Return string representation of color.
        """
        if (number == 0):
            return "green"
        if (((1 <= number) and (number <= 10)) or ((19 <= number) and (number <= 28))):
            if (number % 2 == 0):
                return "black"
            return "red"
        if (((11 <= number) and (number <= 18)) or ((29 <= number) and (number <= 36))):
            if (number % 2 == 0):
                return "red"
            return "black"


class Seed(models.Model):
    """Represent seed for provably random number generation.

    Determines maximal number of usages for single seed, store
    it's char value and specify if seed is visible or not, this
    property is established depending on whether it is still in use.
    """

    MAX_USAGES = 10
    seed = models.CharField(max_length=150)
    visible = models.BooleanField(default=False)

    def __str__(self):
        """Show seed of visible object and 'Hidden' for unvisible ones."""
        if self.visible is True:
            return self.seed
        else:
            return 'Hidden'

    @classmethod
    def generate(cls):
        """Create new unvisible seed element with blank seed char value."""
        object = cls.objects.create()
        return object

    @classmethod
    def get_last_or_none(cls):
        """Change DoesNotExist exception into None or return latest seed."""
        try:
            object = cls.objects.latest('id')
            return object
        except cls.DoesNotExist:
            pass
        return None


class RouletteRound(models.Model):
    """Contain details about wheel spin, win number and generation seed."""

    number = models.IntegerField()
    seed = models.ForeignKey(
        Seed,
        on_delete=models.CASCADE,
    )

    @classmethod
    def go(cls):
        """Contain logic for new winning number generation and reward winning users.

        Firstly try to read latest seed or generate new one, then generate
        new winning number and create new Roulette_Round object. Finally
        look for every not accounted bet and decide if specified user should
        or should not be rewarded, accounted field is being toogled.
        Needed data about winning bets is published for vue framework using
        autobahn module.
        Raise LockException in case of race condition.
        """
        if not mutex.acquire(False):
            raise LockException('Unable to aquire lock')

        latest_seed = Seed.get_last_or_none()
        if (not latest_seed or
                cls.seed_usages(latest_seed.id) >= Seed.MAX_USAGES):
            latest_seed = Seed.generate()

        rand_val = secrets.randbelow(37)
        object = cls.objects.create(seed_id=latest_seed.id, number=rand_val)

        winning_color = Roulette_Bet._number_to_color(object.number)
        winning_choice = object.get_winning_choice()
        all_bets = Roulette_Bet.unaccounted()
        for bet in all_bets:
            bet.accounted = True
            bet.save()
            if bet.choice is winning_choice:
                award_mult = Roulette_Bet._color_to_award_multiplier(
                    winning_color
                )
                award = bet.amount * award_mult
                bet.user_id.addTokens(award)
                bet.user_id.save()
                publish(u'com.casino.updates.user' + str(bet.user_id.id), [
                    {
                        'type': 'tokens_count',
                        'value': bet.user_id.token_count,
                        'delayed': True,
                    },
                    {
                        'type': 'roulette_win',
                        'value': award,
                        'delayed': True
                    },
                ])

        mutex.release()

        return object

    @classmethod
    def seed_usages(cls, id):
        """Return number of usages for specified seed.

        Keyword arguments:
        id -- unique integer defining exactly one seed.
        """
        num = cls.objects.filter(seed__id=id).count()
        return num

    def get_winning_choice(self):
        """Return choice representation of winning color from wining number."""
        color = Roulette_Bet._number_to_color(self.number)
        choice = Roulette_Bet._color_to_choice(color)
        return choice


class Message(models.Model):
    """Represent single user text message for shoutbox functionality."""

    text = models.CharField(max_length=254)
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        """Simply present name of user connected with message and text."""
        return self.user_id.username + ": " + self.text

    # WAMP function
    @classmethod
    def send_message_wamp(cls, user_id, text):
        try:
            user_id = int(user_id)
            user = User.objects.get(id=user_id)
            object = Message.objects.create(user_id=user, text=text)
            publish(u'com.casino.shoutbox', [
                {
                    'type': 'message',
                    'value': {
                        'user_id__username': object.user_id.username,
                        'text': object.text,
                    }
                }
            ])
            return {'type': 'success'}
        except User.DoesNotExist:
            pass
        return {'type': 'unexpected_error'}
