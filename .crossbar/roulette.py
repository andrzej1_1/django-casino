from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner
from os import environ
from twisted.internet.defer import inlineCallbacks
from twisted.internet.task import LoopingCall


class Roulette(ApplicationSession):

    @inlineCallbacks
    def onJoin(self, details):
        def draw():
            self.call(u'com.casino.play_round')
            return True
        LoopingCall(draw).start(20)

        # def add(x, y):
        #     return x + y
        # yield self.register(add, u'com.casino.add')

        yield "[ROULETTE] onJoin completed"


if __name__ == '__main__':
    runner = ApplicationRunner(
        environ.get("AUTOBAHN_DEMO_ROUTER", u"ws://crossbar:8080/ws"),
        u"realm1",
    )
    runner.run(Roulette)
