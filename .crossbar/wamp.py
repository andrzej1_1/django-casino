from __future__ import print_function
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner
from os import environ
from twisted.internet.defer import inlineCallbacks


class Component(ApplicationSession):
    """
    An example application component that register wamp_concat procedure.
    It shows how to integrate crossbar with django.
    """

    @inlineCallbacks
    def onJoin(self, details):
        print("session attached")
        try:
            yield self.register(self.wamp_concat, u'com.casino.wamp_concat')
            print("procedure registered")
        except Exception as e:
            print("could not register procedure: {0}".format(e))

    def wamp_concat(self, a, b):
        result = str(a) + str(b)
        return result


if __name__ == '__main__':
    runner = ApplicationRunner(
        environ.get("AUTOBAHN_DEMO_ROUTER", u"ws://crossbar:8080/ws"),
        u"realm1",
    )
    runner.run(Component)
