# casino

## Setup with docker-arch-suite

1. Download [docker-arch-suite](https://github.com/koxu1996/docker-arch-suite)
2. Set .env variables according to result of `id -u` and `id -g`
2.5 Set variable DAS_PATH pointing to docker-arch-suite directory, for example by adding following line to ~/.profile:
~~~
    export DAS_PATH="/home/andrew/docker-arch-suite"
~~~
3. Place all files under casino folder created in apache-python3/sites
4. Adjust settings in apache-python3/sites/casino/casino/settings_local.py.example eg.:
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': 'mariadb',
        'PORT': '3306',
        'NAME': 'casino',
        'USER': 'root',
        'PASSWORD': 'root',
    }
}
```
then remove '.example' suffix.
5. Create casino.conf in apache-python3/sites-available with following content:
```
<VirtualHost *:80>
    ServerName casino.test
    WSGIScriptAlias / /var/www/html/casino/casino/wsgi.py
    WSGIDaemonProcess casino.test python-path=/var/www/html/casino:/var/www/html/casino
    WSGIProcessGroup casino.test

    Alias /static /var/www/html/casino/static/served

    <Directory /var/www/html/casino/static/served>
        Require all granted
    </Directory>

    <Directory /var/www/html/casino>
        <Files wsgi.py>
        Require all granted
        </Files>
    </Directory>
</VirtualHost>
```
6. Replace existing crossbar volume in docker-common.yml with this one:
```
  - ./apache-python3/sites/casino:/node
```
6.5 Expose crossbar's port 8080:
```
  ports:
    - 8080:8080
```
7. Add volumes and environment to apache-python3 service in docker-common.yml:
```
volumes:               
  ...
  - ${XSOCK}:${XSOCK}:rw                                                                  
  - ${XAUTH}:${XAUTH}:rw
environment:
  ...                                
  - DISPLAY=${DISPLAY}  
  - XAUTHORITY=${XAUTH}
```
8. Adjust ./crossbar/.env:
```
PACMAN_PACKAGES=
PIP_PACKAGES=
```
and ./apache-python3/.env:
```
PACMAN_PACKAGES=libmariadbclient yarn geckodriver firefox
PIP_PACKAGES=django mysqlclient coverage selenium pytest pytest-django pytest-cov stripe autobahn twisted autobahn-sync
```
9. Add these domains to /etc/hosts:
```
127.0.1.1       pma.test    # phpMyAdmin
127.0.1.1       casino.test # casino
127.0.1.1       crossbar # casino
```
10. Run amazing docker-arch-suite:
```
$ ./run.sh
```
Now site is running at http://casino.test

(Remember you can always force to build containers by './run.sh -b' or './run.sh --build')
11. Open phpmyadmin (http://pma.test:8000/) and create database 'casino' with 'utf8_unicode_ci' collation.
12. Open http://casino.test/wamp_init in order to initialize WAMP functions.
13. Go into apache container and change directory:
```
$ docker exec -it dockerarchsuite_apache-python3 bash
$ cd /var/www/html/casino
```
14. Refer to other sections which are explaining how to run checks, add superuser etc.

## Development

These are commands which you should use inside apache-python3 container:

1. Getting frontend dependencies

    $ yarn install

2. Running checks, migrations and publishing files

    $ ./update.sh

3. Creating admin user

    $ python manage.py createsuperuser

4. Building js/css bundle

    $ yarn run build

During development you may need to use actively previous commands. Do not be afraid, you can do it so many times as you need. Also there is one additional, which may be helpful:

    $ touch casino/wsgi.py

It force apache to reload source code of application, so changes are visible without restarting container.

## Testing

Note:
Firstly you have to apply patch from https://github.com/django/django/commit/a9189d27efccdee0b5e8b84f69f9041176055769

Running functional tests:

    1) against test server (recommended for development)

        $ pytest functional_tests/

    2) against real server and existing **production** database

        $ STAGING_SERVER=http://127.0.0.1 pytest functional_tests/

Running unit tests:

    $ pytest website/tests/

--

After test completion you can check code coverage:

    $ firefox coverage_html/index.html

## Production deployment

Set DJANGO_DEBUG_FALSE env variable, define DJANGO_SECRET_KEY and SITENAME

## Debugging

There is logger configured for ERROR level writing to file **/tmp/casino.log**.
You can also emit log manually:

    import logging
    logger = logging.getLogger(__name__)
    logger.error("Sample error!")
