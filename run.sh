#!/usr/bin/env bash

# Get DAS_PATH from env
das_path=${DAS_PATH:?"You have to set DAS_PATH"}

# Check if directory exists
if [ ! -d $das_path ]; then
   echo "Directory $das_path does not exist"
   exit 1
fi

# Open DAS directory
cd $das_path

# Prepare X11
make prepareX11

# Create network if not exists
if [ ! "$(docker network ls --format "{{.Name}}" | grep nginxproxy_default)" ]; then
    docker network create nginxproxy_default
fi

# Run whole stack
if [[ $1 = "--build" ]] || [[ $1 = "-b" ]]; then
    docker-compose -f docker-proxy.yml -f docker-common.yml up --build nginx-proxy apache-python3 mariadb crossbar phpmyadmin
else
    docker-compose -f docker-proxy.yml -f docker-common.yml up nginx-proxy apache-python3 mariadb crossbar phpmyadmin
fi
