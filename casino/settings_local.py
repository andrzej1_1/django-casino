# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': 'mariadb',
        'PORT': '3306',
        'NAME': 'casino',
        'USER': 'root',
        'PASSWORD': 'root',
    }
}
