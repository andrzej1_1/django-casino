from django.test.runner import DiscoverRunner


class UsePyTest(DiscoverRunner):

    def run_tests(self, test_labels, extra_tests=None, **kwargs):
        print("Django test runner is disabled. Use pytest instead.")
