from .base import FunctionalTest, wait
from unittest.mock import patch
from django.utils import timezone
from unittest.mock import patch
from website.models import User, Roulette_Bet, RouletteRound
from selenium.webdriver.support.select import Select

import time


class BetTest(FunctionalTest):

    LOGIN_ERROR = 'You have to be logged!'
    OUT_OF_TOKENS = 'You do not have enough tokens!'

    @wait
    def wait_for_tokens_count(self):
        return self.browser.find_element_by_id('tokens-count')

    @wait
    def wait_for_exact_tokens_count(self, count):
        element = self.wait_for_tokens_count()
        self.assertEqual(element.text, str(count))

    @wait
    def wait_to_login_error(self):
        div = self.browser.find_element_by_id('swal2-content')
        self.assertEqual(div.text, self.LOGIN_ERROR)

    @wait
    def wait_to_out_of_tokens_error(self):
        div = self.browser.find_element_by_id('swal2-content')
        self.assertEqual(div.text, self.OUT_OF_TOKENS)

    def close_sweet_modal(self):
        button = self.browser.find_element_by_css_selector(
            'button.swal2-confirm'
        )
        button.click()

    @wait
    def wait_to_be_logged_out(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign in')

    @wait
    def wait_for_bet_buttons(self):
        self.browser.find_element_by_id('bet_green')
        self.browser.find_element_by_id('bet_red')
        self.browser.find_element_by_id('bet_black')

    def wait_while_wheel_spin(self):
        time.sleep(15)

    def test_cannot_bet(self):
        # Unauthorized user opens '/roulette' page
        self.browser.get(self.live_server_url + '/roulette')
        self.wait_to_be_logged_out()

        welcome = self.browser.find_element_by_id('bet_welcome')
        self.assertEqual(welcome.text, 'Login to bet!')

        # When he click any BetColor, error message shows
        self.browser.find_element_by_id('bet_red').click()
        self.wait_to_login_error()
        self.close_sweet_modal()

        self.browser.find_element_by_id('bet_green').click()
        self.wait_to_login_error()
        self.close_sweet_modal()

        self.browser.find_element_by_id('bet_black').click()
        self.wait_to_login_error()
        self.close_sweet_modal()

    def test_can_bet(self):

        # John has 5 tokens to bet
        tokens_count = 5
        self.create_pre_authenticated_session('john', tokens_count)

        # John open '/roulette' page as logged user
        self.browser.get(self.live_server_url + '/roulette')
        welcome = self.browser.find_element_by_id('bet_welcome')
        self.assertEqual(welcome.text, 'Bet color john!')

        # Now he can click any colored button to bet 1 token
        self.browser.find_element_by_id('bet_red').click()
        self.wait_for_exact_tokens_count(4)

        self.browser.find_element_by_id('bet_green').click()
        self.wait_for_exact_tokens_count(3)

        self.browser.find_element_by_id('bet_black').click()
        self.wait_for_exact_tokens_count(2)

        # Now he decided to increse bet amount
        self.browser.find_element_by_name('rates')
        self.browser.find_element_by_xpath(
            "//select[@id='rates']/option[2]").click()

        self.browser.find_element_by_id('bet_black').click()
        self.wait_to_out_of_tokens_error()
        self.close_sweet_modal()

    @patch('secrets.randbelow')
    def test_can_win_and_lose(self, mock_randbelow):

        # set 0 (green) as draw number
        mock_randbelow.return_value = 0

        # John is registered user with 100 tokens
        john_tokens = 100
        self.create_pre_authenticated_session('john', john_tokens)

        # He open '/roulette' page as logged user
        self.browser.get(self.live_server_url + '/roulette')

        # Then he select amount and place red bet
        currency = Select(self.browser.find_element_by_id("rates"))
        currency.select_by_value('10')
        self.browser.find_element_by_id('bet_red').click()
        john_tokens = john_tokens - 10

        # He also place 25 tokens bet on green
        currency = Select(self.browser.find_element_by_id("rates"))
        currency.select_by_value('25')
        self.browser.find_element_by_id('bet_green').click()
        john_tokens = john_tokens - 25

        # roulette round is processed
        RouletteRound.go()

        # Wheel is spinning
        self.wait_while_wheel_spin()

        # John notice he won 25*2 tokens for green bet
        expected_tokens = john_tokens + (25 * 2)
        self.wait_for_exact_tokens_count(expected_tokens)
