from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import random

from .base import FunctionalTest, wait
from website.models import Currency
from django.test.utils import override_settings

SUCCESS_MSG = 'System zarejestrował wypłatę, '
'jeżeli pieniądze nie zostaną przelane w ciągu najbliższych '
'31 dni roboczych skontaktuj się z administratorem!'

NOT_ENOUGH_TOKENS_MSG = 'Posiadasz zbyt mało tokenów!'

UNAUTHORIZED_MSG = 'Tylko zalogowany użytkownik może dokonać wypłaty!'

PLN_CURRENCY_NAME = 'PLN'

ADDRESS = "Walowa street"


class WithdrawTest(FunctionalTest):

    @wait
    def wait_for_tokens_count(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        return navbar.find_element_by_id('tokens-count').text

    @wait
    def wait_for_withdraw_success(self):
        self.assertIn(SUCCESS_MSG, self.browser.page_source)

    @wait
    def wait_for_withdraw_not_enough_tokens(self):
        self.assertIn(NOT_ENOUGH_TOKENS_MSG, self.browser.page_source)

    @wait
    def wait_for_unauthorized_cannot_withdraw(self):
        self.assertIn(UNAUTHORIZED_MSG, self.browser.page_source)

    @wait
    def wait_to_be_logged_out(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign in')

    @override_settings(DEBUG=True)
    def test_can_exchange_tokens(self):

        # [fixture] currencies
        Currency.objects.create(name=PLN_CURRENCY_NAME, rate=102)
        Currency.objects.create(name="USD", rate=332)

        # John is a logged-in user
        tokens_count = random.randint(2, 100000)
        withdraw_amount = tokens_count - 1
        self.create_pre_authenticated_session('john', tokens_count)

        # He opens home page
        self.browser.get(self.live_server_url)

        # Then clicks 'Withdraw' link
        self.browser.find_element_by_link_text('Withdraw').click()

        # John want to exchange tokens and withdraw PLN
        # so he choose it and enter amount
        currency = Select(self.browser.find_element_by_id("withdraw-currency"))
        currency.select_by_value(PLN_CURRENCY_NAME)
        amount_field = self.browser.find_element_by_id('withdraw-amount')
        amount_field.send_keys(withdraw_amount)
        self.browser.find_element_by_id('address').send_keys(ADDRESS)

        # He submits form by just clicking enter
        amount_field.send_keys(Keys.ENTER)

        # Now he can notice success message and increased number of tokens
        self.wait_for_withdraw_success()
        new_tokens_count = int(self.wait_for_tokens_count())
        expected_delta = withdraw_amount
        self.assertEqual(tokens_count - expected_delta, new_tokens_count)

    @override_settings(DEBUG=True)
    def test_not_enough_tokens(self):

        # [fixture] currencies
        Currency.objects.create(name=PLN_CURRENCY_NAME, rate=102)
        Currency.objects.create(name="USD", rate=332)

        # John is a logged-in user
        withdraw_amount = random.randint(2, 10000)
        tokens_count = withdraw_amount - 1
        self.create_pre_authenticated_session('john', tokens_count)

        # He opens home page
        self.browser.get(self.live_server_url)

        # Then clicks 'Withdraw' link
        self.browser.find_element_by_link_text('Withdraw').click()

        # John want to exchange tokens and withdraw PLN
        # so he choose it and enter amount
        currency = Select(self.browser.find_element_by_id("withdraw-currency"))
        currency.select_by_value(PLN_CURRENCY_NAME)
        amount_field = self.browser.find_element_by_id('withdraw-amount')
        amount_field.send_keys(withdraw_amount)
        self.browser.find_element_by_id('address').send_keys(ADDRESS)

        # He submits form by just clicking enter
        amount_field.send_keys(Keys.ENTER)

        # Now he can notice error message and his tokens amount has not changed
        self.wait_for_withdraw_not_enough_tokens()
        new_tokens_count = int(self.wait_for_tokens_count())
        self.assertEqual(tokens_count, new_tokens_count)

    @override_settings(DEBUG=True)
    def test_logout_user_cannot_withdraw(self):

        # [fixture] currencies
        Currency.objects.create(name=PLN_CURRENCY_NAME, rate=102)
        Currency.objects.create(name="USD", rate=332)

        # Unauthorized user try to withdraw money
        withdraw_amount = random.randint(2, 10000)

        # He opens home page
        self.browser.get(self.live_server_url)

        # He is of course not logged in
        self.wait_to_be_logged_out()

        # Then clicks 'Withdraw' link
        self.browser.find_element_by_link_text('Withdraw').click()

        # Unauthorized want to exchange tokens and withdraw PLN,
        # he is definately thief!
        currency = Select(self.browser.find_element_by_id("withdraw-currency"))
        currency.select_by_value(PLN_CURRENCY_NAME)
        amount_field = self.browser.find_element_by_id('withdraw-amount')
        amount_field.send_keys(withdraw_amount)
        self.browser.find_element_by_id('address').send_keys(ADDRESS)

        # He submits form by just clicking enter
        amount_field.send_keys(Keys.ENTER)

        # Now he can notice error message and his tokens amount has not changed
        self.wait_for_unauthorized_cannot_withdraw()

        # He still cannot be logged in
        self.wait_to_be_logged_out()
