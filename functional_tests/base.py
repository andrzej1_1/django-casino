from django.conf import settings
from django.core import management
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from .management.commands.create_session import (
    create_pre_authenticated_session
)

import os
import time
import io
import pytest
import requests

MAX_WAIT = 10

pytest.wamp_initialized = False


def reset_database_on_server():
    management.call_command('flush', verbosity=0, interactive=False)


def create_session_on_server(username, tokens_count):
    out = io.StringIO()
    management.call_command('create_session', f'--username={username}',
                            f'--tokens_count={tokens_count}', stdout=out)
    return out.getvalue()


def get_staging_server():
    server = os.environ.get('STAGING_SERVER')
    return server


def wait(fn):
    def modified_fn(*args, **kwargs):
        start_time = time.time()
        while True:
            try:
                return fn(*args, **kwargs)
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)
    return modified_fn


class FunctionalTest(StaticLiveServerTestCase):

    def setUp(self):

        self.live_server_url = get_staging_server()
        if not self.live_server_url:
            pytest.exit('Now running functional tests in only possible '
                        'with staging server')

        # If necessary then make request, which will initialize WAMP
        if not pytest.wamp_initialized:
            requests.get(self.live_server_url + '/wamp_init')
            pytest.wamp_initialized = True

        # Reset database
        reset_database_on_server()

        # Run browser
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def create_pre_authenticated_session(self, username, tokens_count):
        self.staging_server = get_staging_server()
        if self.staging_server:
            # functional tests -> against real server
            session_key = create_session_on_server(username, tokens_count)
        else:
            # unit tests -> against fake server
            session_key = create_pre_authenticated_session(username,
                                                           tokens_count)

        # to set a cookie we need to first visit the domain.
        # 404 pages load the quickest!
        self.browser.get(self.live_server_url + "/404_no_such_url/")

        self.browser.add_cookie(dict(
            name=settings.SESSION_COOKIE_NAME,
            value=session_key,
            path='/',
        ))
