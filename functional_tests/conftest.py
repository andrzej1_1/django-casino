from .base import get_staging_server
import pytest
import pytest_django.fixtures


@pytest.fixture(scope='module')
def django_db_setup(
    request,
    django_test_environment,
    django_db_blocker,
    django_db_use_migrations,
    django_db_keepdb,
    django_db_createdb,
    django_db_modify_db_settings,
):
    staging_server = get_staging_server()
    if staging_server:
        print('[WARN] Using production database')
        return

    return pytest_django.fixtures.django_db_setup(
        request,
        django_test_environment,
        django_db_blocker,
        django_db_use_migrations,
        django_db_keepdb,
        django_db_createdb,
        django_db_modify_db_settings,
    )
