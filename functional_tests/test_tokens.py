from .base import FunctionalTest, wait
import random


class TokensTest(FunctionalTest):

    @wait
    def wait_for_tokens_count(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        return navbar.find_element_by_id('tokens-count')

    def test_can_see_tokens_count(self):

        # John is a logged-in user
        tokens_count = random.randint(0, 100000)
        self.create_pre_authenticated_session('john', tokens_count)

        # He opens home page and see his amount of tokens
        self.browser.get(self.live_server_url)
        amount = int(self.wait_for_tokens_count().text)
        self.assertEqual(amount, tokens_count)
