from .base import FunctionalTest, wait
from website.models import RouletteRound


class RouletteRoundTest(FunctionalTest):

    @wait
    def wait_for_numbers_elements_in(self, el):
        numbers = el.find_elements_by_class_name('roulette-number')
        return numbers

    def test_can_get_empty_history_of_draws(self):

        # John open '/roulette' page
        self.browser.get(self.live_server_url + '/roulette')

        # John locate draw history with no content at all
        draw_history = self.browser.find_element_by_id('draw_history')
        self.assertEqual(draw_history.text, '')

    def test_can_see_historic_draws(self):

        # Server run 3 rounds
        round1 = RouletteRound.go()
        round2 = RouletteRound.go()
        round3 = RouletteRound.go()

        # John open '/roulette' page
        self.browser.get(self.live_server_url + '/roulette')

        # John locate all 3 round numbers in history list
        draw_history = self.browser.find_element_by_id('draw_history')
        numbers_els = self.wait_for_numbers_elements_in(draw_history)
        numbers = [el.text for el in numbers_els]

        self.assertEqual(len(numbers), 3)
        self.assertIn(str(round1.number), numbers)
        self.assertIn(str(round2.number), numbers)
        self.assertIn(str(round3.number), numbers)
