from .base import FunctionalTest, wait
from website.models import User, Message
from django.test.utils import override_settings
from selenium.webdriver.common.keys import Keys

import random
import time

FIRST_MSG = 'This is simple, just click withdraw and get money!'
USER_MSG = 'I am bet master!'
UNAUTHORIZED_MSG = 'Only logged user can shout!'
DANGEROUS_MSG = "<script>alert('Injected!');</script>"

TEST_USERNAME_PREVIOUS = 'jash'
TEST_EMAIL_PREVIOUS = 'jash@casino.test'
TEST_PASSWORD_PREVIOUS = 'Tw0jaStaraZ4pierdala'


class ShoutboxTest(FunctionalTest):

    LOGIN_ERROR = 'You have to be logged!'

    @wait
    def wait_for_shout_success(self):
        self.assertIn(FIRST_MSG, self.browser.page_source)
        self.assertIn(USER_MSG, self.browser.page_source)

    @wait
    def wait_for_unauthorized_cannot_shout(self):
        self.assertIn(UNAUTHORIZED_MSG, self.browser.page_source)
        self.assertNotIn(USER_MSG, self.browser.page_source)

    @wait
    def wait_to_be_logged_out(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign in')

    def submit_message(self):
        # imput_field.send_keys(Keys.ENTER) <- not work when window is out of focus
        # Make sure everythin was initialized
        time.sleep(1)
        el = self.browser.find_element_by_id('send_message_button')
        el.click()

    @wait
    def wait_to_login_error(self):
        div = self.browser.find_element_by_id('swal2-content')
        self.assertEqual(div.text, self.LOGIN_ERROR)

    def close_sweet_modal(self):
        button = self.browser.find_element_by_css_selector(
            'button.swal2-confirm'
        )
        button.click()

    @override_settings(DEBUG=True)
    def test_can_read_and_shout(self):

        user = User.objects.create_user(
            username=TEST_USERNAME_PREVIOUS,
            email=TEST_EMAIL_PREVIOUS,
            password=TEST_PASSWORD_PREVIOUS
        )
        Message.objects.create(text=FIRST_MSG, user_id=user)

        # John is a logged-in user
        tokens_count = random.randint(2, 100000)
        self.create_pre_authenticated_session('john', tokens_count)

        # He opens home page
        self.browser.get(self.live_server_url)

        # Then clicks 'Shoutbox' link
        self.browser.find_element_by_link_text('Shoutbox').click()

        # John want to say something as response for jash shout
        text_field = self.browser.find_element_by_id('message-text')
        text_field.send_keys(USER_MSG)

        # He submits message
        self.submit_message()

        # Now he can notice success message and his message
        self.wait_for_shout_success()

    @override_settings(DEBUG=True)
    def test_logout_user_cannot_shout_but_can_read(self):

        # [fixture] currencies
        user = User.objects.create_user(
            username=TEST_USERNAME_PREVIOUS,
            email=TEST_EMAIL_PREVIOUS,
            password=TEST_PASSWORD_PREVIOUS
        )
        Message.objects.create(text=FIRST_MSG, user_id=user)

        # Unauthorized user try to shout, he opens home page
        self.browser.get(self.live_server_url)

        # He is of course not logged in
        self.wait_to_be_logged_out()

        # Then clicks 'Shoutbox' link
        self.browser.find_element_by_link_text('Shoutbox').click()

        # Unauthorized want to shout
        text_field = self.browser.find_element_by_id('message-text')
        text_field.send_keys(USER_MSG)

        # He submits message
        self.submit_message()

        # Now he can notice error message and his message not show up
        self.wait_to_login_error()
        self.close_sweet_modal()

        # He still cannot be logged in
        self.wait_to_be_logged_out()

    @override_settings(DEBUG=True)
    def test_try_js_injection(self):

        # We have begginer hacker user
        self.create_pre_authenticated_session('hacker', 0)
        self.browser.get(self.live_server_url)

        # He clicks 'Shoutbox' link
        self.browser.find_element_by_link_text('Shoutbox').click()

        # Hacker want to js inject with very dangerous code
        text_field = self.browser.find_element_by_id('message-text')
        text_field.send_keys(DANGEROUS_MSG)

        # He submits message
        self.submit_message()

        # But his message is properly sanitized
        self.assertNotIn(DANGEROUS_MSG, self.browser.page_source)
