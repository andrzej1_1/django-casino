from .base import FunctionalTest, wait


class RegistrationTest(FunctionalTest):

    TEST_USERNAME = 'john'
    TEST_FIRSTNAME = 'Johny'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    @wait
    def wait_to_be_logged_in(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Logout')

    @wait
    def wait_to_be_logged_out(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign in')

    def test_can_register(self):
        # Unsigned user open home page and obviously he is not logged
        self.browser.get(self.live_server_url)
        self.wait_to_be_logged_out()

        # He clicks 'Sign up' link
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign up').click()

        # Then he enter account credentials
        self.browser.find_element_by_name(
            'username').send_keys(self.TEST_USERNAME)
        self.browser.find_element_by_name(
            'email').send_keys(self.TEST_EMAIL)
        self.browser.find_element_by_name(
            'first_name').send_keys(self.TEST_FIRSTNAME)
        self.browser.find_element_by_name(
            'password1').send_keys(self.TEST_PASSWORD)
        self.browser.find_element_by_name(
            'password2').send_keys(self.TEST_PASSWORD)
        self.browser.find_element_by_id(
            'create_account').click()

        # John is logged in
        self.wait_to_be_logged_in()

    def test_input_wrong_email(self):
        # Unsigned user open home page and obviously he is not logged
        self.browser.get(self.live_server_url)
        self.wait_to_be_logged_out()

        # He clicks 'Sign up' link
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign up').click()

        # Then he enter account credentials with wrong email format
        self.browser.find_element_by_name(
            'username').send_keys(self.TEST_USERNAME)
        self.browser.find_element_by_name(
            'email').send_keys('wrong')
        self.browser.find_element_by_name(
            'first_name').send_keys(self.TEST_FIRSTNAME)
        self.browser.find_element_by_name(
            'password1').send_keys(self.TEST_PASSWORD)
        self.browser.find_element_by_name(
            'password2').send_keys(self.TEST_PASSWORD)
        self.browser.find_element_by_id(
            'create_account').click()

        # John is not logged in
        self.wait_to_be_logged_out()
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign up').click()

        # He tried again with @
        self.browser.find_element_by_name(
            'username').send_keys(self.TEST_USERNAME)
        self.browser.find_element_by_name(
            'email').send_keys('wrong@')
        self.browser.find_element_by_name(
            'first_name').send_keys(self.TEST_FIRSTNAME)
        self.browser.find_element_by_name(
            'password1').send_keys(self.TEST_PASSWORD)
        self.browser.find_element_by_name(
            'password2').send_keys(self.TEST_PASSWORD)
        self.browser.find_element_by_id(
            'create_account').click()

        # With the same result
        self.wait_to_be_logged_out()
