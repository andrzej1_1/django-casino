from .base import FunctionalTest, wait
from website.models import User
from selenium.webdriver.common.keys import Keys


class LoginTest(FunctionalTest):

    @wait
    def wait_to_be_logged_in(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Logout')

    @wait
    def wait_to_be_logged_out(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign in')

    def test_can_login(self):

        # [fixture] sample user is registered
        TEST_USERNAME = 'john'
        TEST_EMAIL = 'john@casino.test'
        TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'
        User.objects.create_user(username=TEST_USERNAME,
                                 email=TEST_EMAIL,
                                 password=TEST_PASSWORD)

        # John open home page and obviously he is not logged
        self.browser.get(self.live_server_url)
        self.wait_to_be_logged_out()

        # He clicks 'Sing in' link
        self.browser.find_element_by_link_text('Sign in').click()

        # Then he enter account credentials
        self.browser.find_element_by_name('username').send_keys(TEST_USERNAME)
        self.browser.find_element_by_name('password').send_keys(TEST_PASSWORD)
        self.browser.find_element_by_name('password').send_keys(Keys.ENTER)

        # John is logged in
        self.wait_to_be_logged_in()
