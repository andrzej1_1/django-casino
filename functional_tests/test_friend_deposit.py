from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import random

from .base import FunctionalTest, wait
from website.models import Currency, User
from django.test.utils import override_settings

SUCCESS_MSG = 'Płatność została zakończona pomyślnie!'


class FriendDepositTest(FunctionalTest):

    TEST_USERNAME = 'john'
    TEST_EMAIL = 'john@casino.test'
    TEST_PASSWORD = 'Tw0jaStaraZ4pierdala'

    FRIEND_USERNAME = 'jash'
    FRIEND_EMAIL = 'jash@casino.test'
    FRIEND_PASSWORD = 'Ve3yInt21st1ngPass4o5d'

    @wait
    def wait_to_be_logged_in(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Logout')

    @wait
    def wait_to_be_logged_out(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign in')

    @wait
    def wait_for_tokens_count(self):
        navbar = self.browser.find_element_by_css_selector('.navbar')
        return navbar.find_element_by_id('tokens-count').text

    @wait
    def wait_for_deposit_success(self):
        self.assertIn(SUCCESS_MSG, self.browser.page_source)

    @wait
    def switch_to_checkout_iframe(self):
        self.browser.switch_to.frame("stripe_checkout_app")

    @wait
    def fill_payment_data(self):
        self.browser.find_element_by_xpath(
            "(//label[text()='Card number']/parent::div)[1]/input") \
            .send_keys('4242424242424242')
        self.browser.find_element_by_xpath(
            "(//label[text()='Expiry']/parent::div)[1]/input") \
            .send_keys('1250')
        email = self.browser.find_element_by_xpath(
            "(//label[text()='Email']/parent::div)[1]/input"
        )
        email.send_keys('andrzej1_1@o2.pl')
        self.browser.find_element_by_xpath(
            "(//label[text()='CVC']/parent::div)[1]/input") \
            .send_keys('1234')

    def test_no_friend_connection(self):

        # [fixture] currencies
        PLN_CURRENCY_NAME = 'PLN'
        pln_crnc = Currency.objects.create(name=PLN_CURRENCY_NAME, rate=102)
        Currency.objects.create(name="USD", rate=332)

        # John is a casino user
        john_tokens_count = random.randint(0, 100000)
        User.objects.create_user(username=self.TEST_USERNAME,
                            email=self.TEST_EMAIL,
                            password=self.TEST_PASSWORD,
                            token_count=john_tokens_count)

        # Now Jash - who is not john friend - create new account
        self.browser.get(self.live_server_url)
        self.wait_to_be_logged_out()

        # He clicks 'Sign up' link
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign up').click()

        # Then he enter account credentials
        self.browser.find_element_by_name(
            'username').send_keys(self.FRIEND_USERNAME)
        self.browser.find_element_by_name(
            'email').send_keys(self.FRIEND_EMAIL)
        self.browser.find_element_by_name(
            'password1').send_keys(self.FRIEND_PASSWORD)
        self.browser.find_element_by_name(
            'password2').send_keys(self.FRIEND_PASSWORD)
        self.browser.find_element_by_id(
            'create_account').click()

        # Jash is logged in
        self.wait_to_be_logged_in()

        # Now Jash deposit some money
        deposit_amount = random.randint(0, 10000)
        self.browser.find_element_by_link_text('Deposit').click()

        currency = Select(self.browser.find_element_by_id("deposit-currency"))
        currency.select_by_value(PLN_CURRENCY_NAME)
        amount_field = self.browser.find_element_by_id('deposit-amount')
        amount_field.send_keys(deposit_amount)

        # He submits form by just clicking enter
        amount_field.send_keys(Keys.ENTER)

        # Jash fills out Stripe form
        self.switch_to_checkout_iframe()
        self.fill_payment_data()

        # Then clicks submit
        self.browser.find_element_by_class_name('Section-button').click()
        self.browser.switch_to.default_content()

        # now he can notice success message and increased number of tokens
        self.wait_for_deposit_success()
        new_tokens_count = int(self.wait_for_tokens_count())
        expected_delta = deposit_amount * pln_crnc.rate
        self.assertEqual(expected_delta, new_tokens_count)
        self.browser.find_element_by_link_text('Logout').click()
        self.wait_to_be_logged_out()

        # John notices no change in his tokens count
        self.browser.find_element_by_link_text('Sign in').click()
        self.browser.find_element_by_name('username').send_keys(self.TEST_USERNAME)
        self.browser.find_element_by_name('password').send_keys(self.TEST_PASSWORD)
        self.browser.find_element_by_name('password').send_keys(Keys.ENTER)
        self.wait_to_be_logged_in()

        new_john_tokens_count = int(self.wait_for_tokens_count())
        self.assertEqual(john_tokens_count, new_john_tokens_count)

    def test_friend_deposit(self):

        # [fixture] currencies
        PLN_CURRENCY_NAME = 'PLN'
        pln_crnc = Currency.objects.create(name=PLN_CURRENCY_NAME, rate=100)
        Currency.objects.create(name="USD", rate=332)

        # John is a casino user
        john_tokens_count = random.randint(0, 100000)
        User.objects.create_user(username=self.TEST_USERNAME,
                            email=self.TEST_EMAIL,
                            password=self.TEST_PASSWORD,
                            token_count=john_tokens_count)

        # Now Jash - who is john friend - create new account
        self.browser.get(self.live_server_url)
        self.wait_to_be_logged_out()

        # He clicks 'Sign up' link
        navbar = self.browser.find_element_by_css_selector('.navbar')
        navbar.find_element_by_link_text('Sign up').click()

        # Then he enter account credentials with john username in friend input
        self.browser.find_element_by_name(
            'username').send_keys(self.FRIEND_USERNAME)
        self.browser.find_element_by_name(
            'email').send_keys(self.FRIEND_EMAIL)
        self.browser.find_element_by_name(
            'friend').send_keys(self.TEST_USERNAME)
        self.browser.find_element_by_name(
            'password1').send_keys(self.FRIEND_PASSWORD)
        self.browser.find_element_by_name(
            'password2').send_keys(self.FRIEND_PASSWORD)
        self.browser.find_element_by_id(
            'create_account').click()

        # Jash is logged in
        self.wait_to_be_logged_in()

        # Now Jash deposit some money
        deposit_amount = random.randint(1000, 10000)
        self.browser.find_element_by_link_text('Deposit').click()

        currency = Select(self.browser.find_element_by_id("deposit-currency"))
        currency.select_by_value(PLN_CURRENCY_NAME)
        amount_field = self.browser.find_element_by_id('deposit-amount')
        amount_field.send_keys(deposit_amount)

        # He submits form by just clicking enter
        amount_field.send_keys(Keys.ENTER)

        # Jash fills out Stripe form
        self.switch_to_checkout_iframe()
        self.fill_payment_data()

        # Then clicks submit
        self.browser.find_element_by_class_name('Section-button').click()
        self.browser.switch_to.default_content()

        # now he can notice success message and increased number of tokens
        self.wait_for_deposit_success()
        new_tokens_count = int(self.wait_for_tokens_count())
        expected_delta = deposit_amount * pln_crnc.rate
        self.assertEqual(expected_delta, new_tokens_count)
        self.browser.find_element_by_link_text('Logout').click()
        self.wait_to_be_logged_out()

        # John also notices change in his tokens count
        self.browser.find_element_by_link_text('Sign in').click()
        self.browser.find_element_by_name('username').send_keys(self.TEST_USERNAME)
        self.browser.find_element_by_name('password').send_keys(self.TEST_PASSWORD)
        self.browser.find_element_by_name('password').send_keys(Keys.ENTER)
        self.wait_to_be_logged_in()

        expected_delta = new_tokens_count / 10
        new_john_tokens_count = int(self.wait_for_tokens_count())
        self.assertEqual(john_tokens_count + expected_delta,
            new_john_tokens_count)