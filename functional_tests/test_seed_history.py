from .base import FunctionalTest
from website.models import Seed


class SeedTest(FunctionalTest):

    def test_can_get_empty_list_of_server_seeds(self):

        # John open '/seed' page
        self.browser.get(self.live_server_url + '/seed')

        # John locate seed table with no rows (+ one for header)
        seed_table = self.browser.find_element_by_id('seed-table')
        seed_rows = seed_table.find_elements_by_tag_name('tr')

        # -- one row for header
        self.assertEqual(len(seed_rows), 0+1)

    def test_can_get_not_empty_list_of_server_seeds(self):

        # Server generate two seeds
        seed_one = Seed.objects.create(seed="12343", visible=True)
        seed_two = Seed.objects.create(seed="63546456", visible=True)

        # John open '/seed' page
        self.browser.get(self.live_server_url + '/seed')

        # John locate seed table with two rows (+ one for header)
        seed_table = self.browser.find_element_by_id('seed-table')
        seed_rows = seed_table.find_elements_by_tag_name('tr')
        self.assertEqual(len(seed_rows), 2+1)

        # Seed in first row should match with seed_one
        seed_row_one = seed_rows[1]
        seed_one_cols = seed_row_one.find_elements_by_tag_name('td')
        self.assertEqual(seed_one_cols[3].text, seed_one.seed)

        # Seed in second row should match with seed_two
        seed_row_one = seed_rows[2]
        seed_one_cols = seed_row_one.find_elements_by_tag_name('td')
        self.assertEqual(seed_one_cols[3].text, seed_two.seed)

    def test_can_see_only_visible_seeds(self):

        VISIBLE_SEED = '10a0c8103620c7310851e9bb20744dce'
        VISIBLE_SEED2 = 'f081c50f91905c1e342cebc96e4e0ac7'
        HIDDEN_SEED = '4633add95a921cc5d73f3f380709d77a'
        HIDDEN_SEED2 = 'a668ef0fc214c796adc99758035dcf38'

        # Server generate two seeds
        Seed.objects.create(seed=VISIBLE_SEED, visible=True)
        Seed.objects.create(seed=HIDDEN_SEED, visible=False)
        Seed.objects.create(seed=HIDDEN_SEED2, visible=False)
        Seed.objects.create(seed=VISIBLE_SEED2, visible=True)

        # John open '/seed' page
        self.browser.get(self.live_server_url + '/seed')

        # John locate two visible hashes
        self.assertIn(VISIBLE_SEED, self.browser.page_source)
        self.assertIn(VISIBLE_SEED2, self.browser.page_source)

        # John is unable to see hidden seeds
        self.assertNotIn(HIDDEN_SEED, self.browser.page_source)
        self.assertNotIn(HIDDEN_SEED2, self.browser.page_source)
